#ifndef STREAM_H
#define STREAM_H 1

#include <stdio.h>

struct stream {
  FILE *file;
  int width;
  int height;
};

struct stream *stream_new(char *name);
void stream_free(struct stream *s);
int stream_read(struct stream *s);

#endif
