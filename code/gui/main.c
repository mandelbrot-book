#define _POSIX_C_SOURCE 200809L
#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <mpfr.h>
#include <gtk/gtk.h>
#include "mandelbrot_image.h"
#include "mandelbrot_render.h"
#include "mandelbrot_binary_angle.h"
#include "mandelbrot_box_period.h"
#include "mandelbrot_external_ray_in.h"
#include "mandelbrot_external_ray_out.h"
#include "mandelbrot_nucleus.h"
#include "mandelbrot_parent.h"
#include "mandelbrot_interior.h"
#include "mandelbrot_shape.h"
#include "mpfr_complex.h"
#include "pi.h"

static int serialize(const char *filename);
static int deserialize(const char *filename);

struct stack_item {
  struct stack_item *next;
  int value;
};

struct stack {
  struct stack_item *contents;
};

struct stack *stack_new() {
  struct stack *s = calloc(1, sizeof(struct stack));
  return s;
}

int stack_empty(struct stack *s) {
  return s->contents == 0;
}

void stack_push(struct stack *s, int v) {
  struct stack_item *i = calloc(1, sizeof(struct stack_item));
  i->next = s->contents;
  i->value = v;
  s->contents = i;
}

int stack_pop(struct stack *s) {
  struct stack_item *i = s->contents;
  int v = i->value;
  s->contents = i->next;
  free(i);
  return v;
}

void stack_delete(struct stack *s) {
  while (! stack_empty(s)) { stack_pop(s); }
  free(s);
}

struct view {
  mpfr_t cx, cy, radius, pixel_spacing;
};

static struct view *view_new() {
  struct view *v = malloc(sizeof(struct view));
  mpfr_init2(v->cx, 53);
  mpfr_init2(v->cy, 53);
  mpfr_init2(v->radius, 53);
  mpfr_init2(v->pixel_spacing, 53);
  return v;
}

static void view_delete(struct view *v) {
  mpfr_clear(v->cx);
  mpfr_clear(v->cy);
  mpfr_clear(v->radius);
  mpfr_clear(v->pixel_spacing);
  free(v);
}

static struct view *view_copy(const struct view *v) {
  struct view *v2 = view_new();
  mpfr_set_prec(v2->cx, mpfr_get_prec(v->cx));
  mpfr_set_prec(v2->cy, mpfr_get_prec(v->cy));
  mpfr_set(v2->cx, v->cx, GMP_RNDN);
  mpfr_set(v2->cy, v->cy, GMP_RNDN);
  mpfr_set(v2->radius, v->radius, GMP_RNDN);
  return v2;
}

struct point {
  struct point *next;
  mpfr_t x;
  mpfr_t y;
};

enum annotation_t {
  annotation_ray_out,
  annotation_ray_in,
  annotation_text
};

struct annotation_ray_out {
  struct point *line;
};

struct annotation_ray_in {
  struct point *line;
  struct mandelbrot_binary_angle *angle;
  struct mandelbrot_external_ray_in *ray;
  int depth;
/*
  int have_nucleus;
  int have_landing;
  mpfr_t nucleusx, nucleusy, landingx, landingy, eps_2;
*/
};

struct annotation_text {
  mpfr_t x, y;
};

struct annotation {
  struct annotation *next;
  char *label;
  int selected;
  enum annotation_t tag;
  union {
    struct annotation_ray_out ray_out;
    struct annotation_ray_in ray_in;
    struct annotation_text text;
  } u;
};

static struct point *point_new(struct point *next) {
  struct point *l = malloc(sizeof(struct point));
  l->next = next;
  mpfr_init2(l->x, 53);
  mpfr_init2(l->y, 53);
  return l;
}

static void points_delete(struct point *l) {
  struct point *next = 0;
  do {
    next = l->next;
    mpfr_clear(l->x);
    mpfr_clear(l->y);
    free(l);
  } while ((l = next));
}

static struct {
  cairo_surface_t *surface;
  int width, height;
  GtkWidget *da, *window, *log;
  // zoom tool
  gulong zoom_motion_notify_handler, zoom_button_press_handler, zoom_button_release_handler;
  // info tool
  gulong info_button_press_handler;
  // ray out tool
  gulong ray_out_button_press_handler;
  // nucleus tool
  gulong nucleus_motion_notify_handler, nucleus_button_press_handler, nucleus_button_release_handler;
  // bond tool
  gulong bond_button_press_handler;
  // transient box drawing
  int box;
  double box_aspect, box_x1, box_y1, box_x2, box_y2;
  // annotations
  GtkTreeStore *annostore;
  GtkWidget *annotree;
  struct annotation *anno;
  // mandelbrot view
  struct view *view;
  mpfr_t escape_radius2;
  int maximum_iterations;
  struct mandelbrot_image *image;
  // async computations
  double progress;
  int cancelled;
  GtkWidget *dialog, *bar;
} G;

static void screen_to_param(double x, double y, mpfr_t cx, mpfr_t cy) {
  double sx = (x - G.width / 2.0) / (G.height / 2.0);
  double sy = (G.height / 2.0 - y) / (G.height / 2.0);
  mpfr_set_prec(cx, mpfr_get_prec(G.view->cx));
  mpfr_set_prec(cy, mpfr_get_prec(G.view->cy));
  mpfr_set_d(cx, sx, GMP_RNDN);
  mpfr_set_d(cy, sy, GMP_RNDN);
  mpfr_mul(cx, cx, G.view->radius, GMP_RNDN);
  mpfr_mul(cy, cy, G.view->radius, GMP_RNDN);
  mpfr_add(cx, cx, G.view->cx, GMP_RNDN);
  mpfr_add(cy, cy, G.view->cy, GMP_RNDN);
}

static void param_to_screen(double *x, double *y, mpfr_t cx, mpfr_t cy) {
  mpfr_t tx, ty;
  mpfr_init2(tx, mpfr_get_prec(G.view->cx));
  mpfr_init2(ty, mpfr_get_prec(G.view->cy));
  mpfr_sub(tx, cx, G.view->cx, GMP_RNDN);
  mpfr_sub(ty, cy, G.view->cy, GMP_RNDN);
  mpfr_div(tx, tx, G.view->radius, GMP_RNDN);
  mpfr_div(ty, ty, G.view->radius, GMP_RNDN);
  double sx = mpfr_get_d(tx, GMP_RNDN);
  double sy = mpfr_get_d(ty, GMP_RNDN);
  *x = sx * G.height / 2.0 + G.width / 2.0;
  *y = G.height / 2.0 - sy * G.height / 2.0;
  mpfr_clear(tx);
  mpfr_clear(ty);
}

static void log_append(const char *text) {
  GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(G.log));
  GtkTextIter end;
  gtk_text_buffer_get_iter_at_offset(buf, &end, -1);
  gtk_text_buffer_insert(buf, &end, text, -1);
  gtk_text_buffer_get_iter_at_offset(buf, &end, -1);
  GtkTextMark *eof = gtk_text_buffer_get_mark(buf, "EOF");
  if (eof) {
    gtk_text_buffer_move_mark(buf, eof, &end);
  } else {
    eof = gtk_text_buffer_create_mark(buf, "EOF", &end, FALSE);
  }
  gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW(G.log), eof, 0.0, FALSE, 0.0, 0.0);
}

static gboolean log_append_thread(void *text) {
  log_append(text);
  free(text);
  return G_SOURCE_REMOVE;
}

static gboolean render_thread_progress(void *userdata) {
  (void) userdata;
  if (! G.cancelled)
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (G.bar), G.progress);
  return G_SOURCE_REMOVE;
}

static int progresscb(void *userdata, double progress) {
  (void) userdata;
  G.progress = progress;
  gdk_threads_add_idle (render_thread_progress, 0);
  return ! G.cancelled;
}

static void dorender(struct view *v, struct mandelbrot_image *image) {

  mpfr_div_d(v->pixel_spacing, v->radius, G.height / 2.0, GMP_RNDN);
  mpfr_t pixel_spacing_log;
  mpfr_init2(pixel_spacing_log, 53);
  mpfr_log2(pixel_spacing_log, v->pixel_spacing, GMP_RNDN);
  int pixel_spacing_bits = -mpfr_get_d(pixel_spacing_log, GMP_RNDN);
  mpfr_clear(pixel_spacing_log);

  int interior = 1;
  int float_type = 1;
  if (interior) {
    if (pixel_spacing_bits > 50 / 2) {
      float_type = 2;
    }
    if (pixel_spacing_bits > 60 / 2) {
      float_type = 3;
    }
  } else {
    if (pixel_spacing_bits > 50) {
      float_type = 2;
    }
    if (pixel_spacing_bits > 60) {
      float_type = 3;
    }
  }

  const char *float_type_str = 0;
  switch (float_type) {
    case 0: float_type_str = "float"; break;
    case 1: float_type_str = "double"; break;
    case 2: float_type_str = "long double"; break;
    case 3: float_type_str = "mpfr"; break;
    default: float_type_str = "?"; break;
  }

  char *buf = calloc(1, 65536);
  mpfr_snprintf(buf, 65536,
    "\nRENDER\n"
    "real: %Re\n"
    "imag: %Re\n"
    "radius: %Re\n"
    "bits: %d\n"
    "type: %s",
    v->cx, v->cy, v->radius, pixel_spacing_bits, float_type_str);
  gdk_threads_add_idle(log_append_thread, buf);

  switch (float_type) {
    case 0:
      mandelbrot_renderf(image, mpfr_get_d(v->cx, GMP_RNDN) + I * mpfr_get_d(v->cy, GMP_RNDN), mpfr_get_d(v->radius, GMP_RNDN), G.maximum_iterations, mpfr_get_d(G.escape_radius2, GMP_RNDN), mpfr_get_d(v->pixel_spacing, GMP_RNDN), interior, 0, progresscb);
      break;
    case 1:
      mandelbrot_render(image, mpfr_get_d(v->cx, GMP_RNDN) + I * mpfr_get_d(v->cy, GMP_RNDN), mpfr_get_d(v->radius, GMP_RNDN), G.maximum_iterations, mpfr_get_d(G.escape_radius2, GMP_RNDN), mpfr_get_d(v->pixel_spacing, GMP_RNDN), interior, 0, progresscb);
      break;
    case 2:
      mandelbrot_renderl(image, mpfr_get_ld(v->cx, GMP_RNDN) + I * mpfr_get_ld(v->cy, GMP_RNDN), mpfr_get_ld(v->radius, GMP_RNDN), G.maximum_iterations, mpfr_get_ld(G.escape_radius2, GMP_RNDN), mpfr_get_ld(v->pixel_spacing, GMP_RNDN), interior, 0, progresscb);
      break;
    case 3:
      mandelbrot_render_mpfr(image, v->cx, v->cy, v->radius, G.maximum_iterations, G.escape_radius2, v->pixel_spacing, interior, 0, progresscb);
      break;
  }
}

static int channel(float c) {
  return fminf(fmaxf(roundf(255 * c), 0), 255);
}

static void hsv2rgb(float h, float s, float v, float *red, float *grn, float *blu) {
  float i, f, p, q, t, r, g, b;
  if (s == 0) { r = g = b = v; } else {
    h = 6 * (h - floorf(h));
    int ii = i = floorf(h);
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0: r = v; g = t; b = p; break;
      case 1: r = q; g = v; b = p; break;
      case 2: r = p; g = v; b = t; break;
      case 3: r = p; g = q; b = v; break;
      case 4: r = t; g = p; b = v; break;
      default:r = v; g = p; b = q; break;
    }
  }
  *red = r;
  *grn = g;
  *blu = b;
}

static void colour(int *r, int *g, int *b, int final_n, float final_z_abs, float final_z_arg, float de, int final_p, int t) {
  float continuous_escape_time = final_n - log2f(final_z_abs + 1.0f);
  (void) continuous_escape_time;
  float radius = 0.0f, angle = 0.0f;
  if (t > 0) {
    radius = final_z_abs;
    angle = final_z_arg;
  } else if (t < 0) {
    radius = -log2f(1.0f - final_z_abs);
    angle = fmodf(powf(2.0, floorf(radius) + 3.0f) * final_z_arg, 1.0f);
    radius = 1.0 - fmodf(radius, 1.0f);
  }
  float k = powf(0.5f, 0.5f - radius);
  float grid_weight = 0.05f;
  int grid =
    grid_weight     < radius && radius < 1.0f - grid_weight &&
    grid_weight * k < angle  && angle  < 1.0f - grid_weight * k;
  float hue = (final_p - 1.0f) / 24.618033988749895f;
  float sat = final_p > 0.0f ? 0.5f : 0.0f;
  float val = fmin(tanh(fmin(fmax(de, 0.0), 4.0)), 0.9 + 0.1 * grid);
  if (t == 0) {
    hue = 0.0f;
    sat = 0.0f;
    val = 0.0f;
  }
  float red, grn, blu;
  hsv2rgb(hue, sat, val, &red, &grn, &blu);
  *r = channel(red);
  *g = channel(grn);
  *b = channel(blu);
}

static void docolour(struct mandelbrot_image *image) {
  if (!G.surface) {
    return;
  }
  cairo_surface_flush(G.surface);
  uint32_t *data = (uint32_t *) cairo_image_surface_get_data(G.surface);
  int channels = 4;
  int stride = cairo_image_surface_get_stride(G.surface) / channels;
  #pragma omp parallel for
  for (int j = 0; j < image->height; ++j) {
    for (int i = 0; i < image->width; ++i) {
      int r, g, b, final_n = 0, final_p = 0, t = 0;
      float final_z_abs = 0, final_z_arg = 0, de = 0;
      complex float z = 0;
      struct mandelbrot_pixel *p = mandelbrot_image_peek(image, i, j);
      switch (p->tag) {
        case mandelbrot_pixel_exterior:
          z = p->u.exterior.final_z;
          final_z_abs = logf(cabsf(z)) / logf(sqrtf(mpfr_get_d(G.escape_radius2, GMP_RNDN))) - 1.0f;
          final_z_arg = fmodf(cargf(z) / (2.0f * pif) + 1.0f, 1.0f);
          de = p->u.exterior.final_de;
          final_p = p->u.exterior.final_p;
          final_n = p->u.exterior.final_n;
          t = 1;
          break;
        case mandelbrot_pixel_interior:
          z = p->u.interior.final_dz;
          final_z_abs = cabsf(z);
          final_z_arg = fmodf(cargf(z) / (2.0f * pif) + 1.0f, 1.0f);
          de = p->u.interior.final_de;
          final_p = p->u.interior.final_p;
          t = -1;
          break;
        default:
          break;
      }
      colour(&r, &g, &b, final_n, final_z_abs, final_z_arg, de, final_p, t);
      int px = (r << 16) | (g << 8) | b;
      int k = j * stride + i;
      data[k] = (0xFF << 24) | px;
    }
  }
  cairo_surface_mark_dirty(G.surface);
}

static gboolean render_thread_cancelled(gpointer user_data) {
  (void) user_data;
  G.dialog = 0;
  G.bar = 0;
  log_append("\nCANCELLED");
  return G_SOURCE_REMOVE;
}

static gboolean render_thread_done(gpointer user_data) {
  (void) user_data;
  gtk_widget_destroy(G.dialog);
  G.dialog = 0;
  G.bar = 0;
  gtk_widget_queue_draw(G.da);
  return G_SOURCE_REMOVE;
}

struct render_data {
  struct view *view;
  struct mandelbrot_image *image;
};

static gpointer render_thread(gpointer user_data) {
  struct render_data *d = user_data;
  if (! G.cancelled) {
    dorender(d->view, d->image);
  }
  int cancelled = G.cancelled;
  if (! cancelled) {
    docolour(d->image);
  }
  if (cancelled) {
    view_delete(d->view);
    mandelbrot_image_free(d->image);
    gdk_threads_add_idle(render_thread_cancelled, 0);
  } else {
    if (G.view) {
      view_delete(G.view);
    }
    G.view = d->view;
    if (G.image) {
      mandelbrot_image_free(G.image);
    }
    G.image = d->image;
    gdk_threads_add_idle(render_thread_done, 0);
  }
  free(d);
  return 0;
}

static void start_render(struct view *v) {

  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  G.dialog = gtk_dialog_new_with_buttons("Progress", GTK_WINDOW(G.window), flags, NULL, NULL);
  GtkWidget *box = gtk_dialog_get_content_area(GTK_DIALOG(G.dialog));
  G.bar = gtk_progress_bar_new();
  gtk_container_add(GTK_CONTAINER(box), G.bar);

  struct render_data *d = malloc(sizeof(struct render_data));
  d->view = v;
  d->image = mandelbrot_image_new(G.width, G.height);
  G.cancelled = 0;
  G.progress = 0;

  GThread *thread = g_thread_new("render", render_thread, d);

  gtk_widget_show_all(G.dialog);
  gtk_dialog_run(GTK_DIALOG(G.dialog));

  G.cancelled = 1;
  g_thread_join(thread);
  if (G.dialog) {
    gtk_widget_destroy(G.dialog);
  }
}

struct ray_out_data {
  int dwell;
  int period;
  struct mandelbrot_external_ray_out *ray;
  struct stack *bits;
  struct annotation *anno;
};

static gboolean ray_out_progress(void *userdata) {
  (void) userdata;
  if (! G.cancelled) {
    gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(G.bar), G.progress);
  }
  return G_SOURCE_REMOVE;
}

static int ray_out_progress_thread(double progress) {
  G.progress = progress;
  gdk_threads_add_idle(ray_out_progress, 0);
  return ! G.cancelled;
}

static gboolean ray_out_thread_cancelled(gpointer user_data) {
  (void) user_data;
  G.dialog = 0;
  G.bar = 0;
  return G_SOURCE_REMOVE;
}

static gboolean ray_out_thread_done(gpointer user_data) {
  (void) user_data;
  gtk_widget_destroy(G.dialog);
  G.dialog = 0;
  G.bar = 0;
  gtk_widget_queue_draw(G.da);
  return G_SOURCE_REMOVE;
}

static gboolean add_annotation(void *userdata) {
  struct annotation *ls = userdata;
  ls->next = G.anno;
  G.anno = ls;
  GtkTreeIter iter;
  gtk_tree_store_append(G.annostore, &iter, NULL);
  gtk_tree_store_set(G.annostore, &iter, 0, ls->label, 1, ls, -1);
  return G_SOURCE_REMOVE;
}

static void add_annotation_thread(void *userdata) {
  gdk_threads_add_idle(add_annotation, userdata);
}

static void delete_annotation(struct annotation *a) {
  struct annotation *prev = 0;
  for (struct annotation *ls = G.anno; ls; ls = ls->next) {
    if (ls->next == a) {
      prev = ls;
      break;
    }
  }
  if (prev) {
    prev->next = prev->next->next;
  } else {
    if (G.anno == a) {
      G.anno = G.anno->next;
    }
  }
  free(a->label);
  switch (a->tag) {
    case annotation_ray_out:
      points_delete(a->u.ray_out.line);
      break;
    case annotation_ray_in:
      points_delete(a->u.ray_in.line);
      mandelbrot_external_ray_in_delete(a->u.ray_in.ray);
      mandelbrot_binary_angle_delete(a->u.ray_in.angle);
/*
      mpfr_clear(a->u.ray_in.nucleusx);
      mpfr_clear(a->u.ray_in.nucleusy);
      mpfr_clear(a->u.ray_in.landingx);
      mpfr_clear(a->u.ray_in.landingy);
      mpfr_clear(a->u.ray_in.eps_2);
*/
      break;
    case annotation_text:
      mpfr_clear(a->u.text.x);
      mpfr_clear(a->u.text.y);
      break;
  }
  free(a);
}

static gpointer ray_out_thread(gpointer userdata) {
  struct ray_out_data *d = userdata;
  int i = 0, n = 0;
  while (! G.cancelled && mandelbrot_external_ray_out_step(d->ray)) {
    if (mandelbrot_external_ray_out_have_bit(d->ray)) {
      stack_push(d->bits, mandelbrot_external_ray_out_get_bit(d->ray));
      ++n;
    }
    d->anno->u.ray_out.line = point_new(d->anno->u.ray_out.line);
    mandelbrot_external_ray_out_get(d->ray, d->anno->u.ray_out.line->x, d->anno->u.ray_out.line->y);
    ray_out_progress_thread(++i / (16.0 * d->dwell)); // FIXME library sharpness
  }
  if (! G.cancelled) {
    char *bitstring = malloc(n + 1);
    for (int j = 0; j < n; ++j) {
      bitstring[j] = '0' + stack_pop(d->bits);
    }
    bitstring[n] = 0;
    d->anno->label = bitstring;
    mandelbrot_external_ray_out_delete(d->ray);
    stack_delete(d->bits);
    add_annotation_thread(d->anno);
    free(d);
    gdk_threads_add_idle(ray_out_thread_done, 0);
  } else {
    mandelbrot_external_ray_out_delete(d->ray);
    stack_delete(d->bits);
    points_delete(d->anno->u.ray_out.line);
    free(d->anno->label);
    free(d->anno);
    free(d);
    gdk_threads_add_idle(ray_out_thread_cancelled, 0);
  }
  return 0;
}

static void start_ray_out(double x, double y) {
  int i = x, j = y;
  if (! mandelbrot_image_in_bounds(G.image, i, j)) {
    return;
  }
  struct mandelbrot_pixel *px = mandelbrot_image_peek(G.image, i, j);
  if (px->tag != mandelbrot_pixel_exterior) {
    return;
  }
  int dwell = px->u.exterior.final_n;
  int period = px->u.exterior.final_p;
  mpfr_t cx, cy;
  mpfr_init2(cx, 53);
  mpfr_init2(cy, 53);
  screen_to_param(x, y, cx, cy);
  struct ray_out_data *d = malloc(sizeof(struct ray_out_data));
  d->dwell = dwell;
  d->period = period;
  d->ray = mandelbrot_external_ray_out_new(cx, cy);
  d->bits = stack_new();
  d->anno = malloc(sizeof(struct annotation));
  d->anno->next = 0;
  d->anno->selected = 0;
  d->anno->label = 0;
  d->anno->tag = annotation_ray_out;
  d->anno->u.ray_out.line = point_new(NULL);
  mpfr_set_prec(d->anno->u.ray_out.line->x, mpfr_get_prec(cx));
  mpfr_set_prec(d->anno->u.ray_out.line->y, mpfr_get_prec(cy));
  mpfr_set(d->anno->u.ray_out.line->x, cx, GMP_RNDN);
  mpfr_set(d->anno->u.ray_out.line->y, cy, GMP_RNDN);

  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  G.dialog = gtk_dialog_new_with_buttons("Progress", GTK_WINDOW(G.window), flags, NULL, NULL);
  GtkWidget *box = gtk_dialog_get_content_area(GTK_DIALOG(G.dialog));
  G.bar = gtk_progress_bar_new();
  gtk_container_add(GTK_CONTAINER(box), G.bar);

  G.cancelled = 0;
  G.progress = 0;
  GThread *thread = g_thread_new("ray_out", ray_out_thread, d);

  gtk_widget_show_all(G.dialog);
  gtk_dialog_run(GTK_DIALOG(G.dialog));

  G.cancelled = 1;
  g_thread_join(thread);
  if (G.dialog) {
    gtk_widget_destroy(G.dialog);
  }
}

struct ray_in_data {
  struct annotation *anno;
  int depth;
  int extending;
};

static gboolean ray_in_progress(gpointer userdata) {
  (void) userdata;
  if (! G.cancelled) {
    gtk_progress_bar_pulse(GTK_PROGRESS_BAR(G.bar));
  }
  return G_SOURCE_REMOVE;
}

static void ray_in_progress_thread(void) {
  gdk_threads_add_idle(ray_in_progress, 0);
}

static gboolean ray_in_thread_cancelled(gpointer user_data) {
  (void) user_data;
  G.dialog = 0;
  G.bar = 0;
  return G_SOURCE_REMOVE;
}

static gboolean ray_in_thread_done(gpointer user_data) {
  (void) user_data;
  gtk_widget_destroy(G.dialog);
  G.dialog = 0;
  G.bar = 0;
  gtk_widget_queue_draw(G.da);
  return G_SOURCE_REMOVE;
}

static gpointer ray_in_thread(gpointer userdata) {
  struct ray_in_data *d = userdata;
/*
  int period = mandelbrot_binary_angle_period(d->anno->u.ray_in.angle);
  int periodic = mandelbrot_binary_angle_preperiod(d->anno->u.ray_in.angle) == 0;
*/
  int n = 0;
/*
#define VARS zx, zy, cx, cy, z2, mz2, t0, t1, t2, t3
  mpfr_t VARS;
  mpfr_inits2(53, VARS, (mpfr_ptr) 0);
*/
  while (! G.cancelled && n < 4 * d->depth && mandelbrot_external_ray_in_step(d->anno->u.ray_in.ray)) { // FIXME library sharpness
    d->anno->u.ray_in.line = point_new(d->anno->u.ray_in.line);
    mandelbrot_external_ray_in_get(d->anno->u.ray_in.ray, d->anno->u.ray_in.line->x, d->anno->u.ray_in.line->y);
    n++;
/*
    if (periodic) {
      if (n >= 4 * period && ! d->anno->u.ray_in.have_nucleus) { // FIXME library sharpness
        int mp = 0;
        mpfr_prec_t p = mpfr_get_prec(G.view->cx);
        mpfr_set_prec(zx, p);
        mpfr_set_prec(zy, p);
        mpfr_set_prec(cx, p);
        mpfr_set_prec(cy, p);
        mpfr_set_prec(z2, p);
        mpfr_set_prec(mz2, p);
        mpfr_set_prec(t0, p);
        mpfr_set_prec(t1, p);
        mpfr_set_prec(t2, p);
        mpfr_set_prec(t3, p);
        mpfr_set_d(zx, 0.0, GMP_RNDN);
        mpfr_set_d(zy, 0.0, GMP_RNDN);
        mpfr_set(cx, d->anno->u.ray_in.line->x, GMP_RNDN);
        mpfr_set(cy, d->anno->u.ray_in.line->y, GMP_RNDN);
        mpfr_set_d(mz2, 1.0/0.0, GMP_RNDN);
        for (int i = 0; i < period; ++i) {
          cmpfr_sqr(zx, zy, zx, zy, t0, t1, GMP_RNDN);
          cmpfr_add(zx, zy, zx, zy, cx, cy, GMP_RNDN);
          cmpfr_abs2(z2, zx, zy, t0, t1, GMP_RNDN);
          if (mpfr_less_p(z2, mz2)) {
            mp = i + 1;
            mpfr_set(mz2, z2, GMP_RNDN);
          }
        }
        if (mp == period) {
          mandelbrot_nucleus(d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, cx, cy, period, 64);
          d->anno->u.ray_in.have_nucleus = 1;
          switch (mandelbrot_shape(d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, period)) {
            case mandelbrot_shape_cardioid: {
              mpfr_set_d(zx, 1.0, GMP_RNDN);
              mpfr_set_d(zy, 0.0, GMP_RNDN);
              mpfr_t wux, wuy;
              mpfr_init2(wux, mpfr_get_prec(d->anno->u.ray_in.nucleusx));
              mpfr_init2(wuy, mpfr_get_prec(d->anno->u.ray_in.nucleusx));
              mandelbrot_interior(wux, wuy, d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, period, zx, zy, 64);
              d->anno->u.ray_in.have_landing = 1;
              mpfr_clear(wux);
              mpfr_clear(wuy);
              break;
            }
            case mandelbrot_shape_circle: {
              mpfr_t wux, wuy;
              mpfr_init2(wux, mpfr_get_prec(d->anno->u.ray_in.nucleusx));
              mpfr_init2(wuy, mpfr_get_prec(d->anno->u.ray_in.nucleusx));
              complex double t = cexp(I * 0.001);
              mpfr_set_d(zx, creal(t), GMP_RNDN);
              mpfr_set_d(zy, cimag(t), GMP_RNDN);
              mandelbrot_interior(wux, wuy, t0, t1, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, period, zx, zy, 64);
              t = cexp(I * -0.001);
              mpfr_set_d(zx, creal(t), GMP_RNDN);
              mpfr_set_d(zy, cimag(t), GMP_RNDN);
              mandelbrot_interior(wux, wuy, t2, t3, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, d->anno->u.ray_in.nucleusx, d->anno->u.ray_in.nucleusy, period, zx, zy, 64);
              cmpfr_add(d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, t0, t1, t2, t3, GMP_RNDN);
              cmpfr_mul_2si(d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, -1, GMP_RNDN);
              d->anno->u.ray_in.have_landing = 1;
              mpfr_clear(wux);
              mpfr_clear(wuy);
              break;
            }
          }
        }
      }
    }
    if (d->anno->u.ray_in.have_landing) {
      mpfr_prec_t p = mpfr_get_prec(d->anno->u.ray_in.line->x);
      mpfr_set_prec(zx, p);
      mpfr_set_prec(zy, p);
      mpfr_set_prec(z2, p);
      mpfr_set_prec(t0, p);
      mpfr_set_prec(t1, p);
      cmpfr_sub(zx, zy, d->anno->u.ray_in.line->x, d->anno->u.ray_in.line->y, d->anno->u.ray_in.landingx, d->anno->u.ray_in.landingy, GMP_RNDN);
      cmpfr_abs2(z2, zx, zy, t0, t1, GMP_RNDN);
      if (mpfr_less_p(z2, d->anno->u.ray_in.eps_2)) {
        break;
      }
    }
*/
    if (n % 100 == 0) {
      ray_in_progress_thread();
    }
  }
/*
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
*/
  d->anno->u.ray_in.depth += n / 4; // FIXME library sharpness
  if (! G.cancelled) {
    if (! d->extending) {
      add_annotation_thread(d->anno);
    }
    free(d);
    gdk_threads_add_idle(ray_in_thread_done, 0);
  } else {
    if (! d->extending) {
      delete_annotation(d->anno);
    }
    free(d);
    gdk_threads_add_idle(ray_in_thread_cancelled, 0);
  }
  return 0;
}

static void start_ray_in(struct mandelbrot_binary_angle *angle, int depth) {
  struct ray_in_data *d = malloc(sizeof(struct ray_in_data));
  d->depth = depth;
  d->extending = 0;
  d->anno = malloc(sizeof(struct annotation));
  d->anno->next = 0;
  d->anno->selected = 0;
  d->anno->label = mandelbrot_binary_angle_to_string(angle);
  d->anno->tag = annotation_ray_in;
  d->anno->u.ray_in.line = 0;
  d->anno->u.ray_in.angle = angle;
  d->anno->u.ray_in.depth = 0;
  mpq_t q;
  mpq_init(q);
  mandelbrot_binary_angle_to_rational(q, angle);
  d->anno->u.ray_in.ray = mandelbrot_external_ray_in_new(q);
  mpq_clear(q);
/*
  d->anno->u.ray_in.have_nucleus = 0;
  d->anno->u.ray_in.have_landing = 0;
  mpfr_init2(d->anno->u.ray_in.nucleusx, 53);
  mpfr_init2(d->anno->u.ray_in.nucleusy, 53);
  mpfr_init2(d->anno->u.ray_in.landingx, 53);
  mpfr_init2(d->anno->u.ray_in.landingy, 53);
  mpfr_init2(d->anno->u.ray_in.eps_2, 53);
  mpfr_set(d->anno->u.ray_in.eps_2, G.view->pixel_spacing, GMP_RNDN);
  mpfr_sqr(d->anno->u.ray_in.eps_2, d->anno->u.ray_in.eps_2, GMP_RNDN);
*/

  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  G.dialog = gtk_dialog_new_with_buttons("Progress", GTK_WINDOW(G.window), flags, NULL, NULL);
  GtkWidget *box = gtk_dialog_get_content_area(GTK_DIALOG(G.dialog));
  G.bar = gtk_progress_bar_new();
  gtk_container_add(GTK_CONTAINER(box), G.bar);

  G.cancelled = 0;
  G.progress = 0;

  GThread *thread = g_thread_new("ray_in", ray_in_thread, d);
  gtk_widget_show_all(G.dialog);
  gtk_dialog_run(GTK_DIALOG(G.dialog));

  G.cancelled = 1;
  g_thread_join(thread);
  if (G.dialog) {
    gtk_widget_destroy(G.dialog);
  }
}

static void start_ray_in_extend(struct annotation *anno, int depth) {
  struct ray_in_data *d = malloc(sizeof(struct ray_in_data));
  d->anno = anno;
  d->depth = depth;
  d->extending = 1;

  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  G.dialog = gtk_dialog_new_with_buttons("Progress", GTK_WINDOW(G.window), flags, NULL, NULL);
  GtkWidget *box = gtk_dialog_get_content_area(GTK_DIALOG(G.dialog));
  G.bar = gtk_progress_bar_new();
  gtk_container_add(GTK_CONTAINER(box), G.bar);

  G.cancelled = 0;
  G.progress = 0;

  GThread *thread = g_thread_new("ray_in", ray_in_thread, d);
  gtk_widget_show_all(G.dialog);
  gtk_dialog_run(GTK_DIALOG(G.dialog));

  G.cancelled = 1;
  g_thread_join(thread);
  if (G.dialog) {
    gtk_widget_destroy(G.dialog);
  }
}


static void box_rectangle(double *x, double *y, double *w, double *h) {
  double dx = fabs(G.box_x2 - G.box_x1);
  double dy = fabs(G.box_y2 - G.box_y1);
  if (dx <= G.box_aspect * dy) {
    *h = dy;
    *w = *h * G.box_aspect;
  } else {
    *w = dx;
    *h = *w / G.box_aspect;
  }
  *x = G.box_x1 - *w;
  *y = G.box_y1 - *h;
  *w *= 2;
  *h *= 2;
}

static void draw_box(cairo_t *cr) {
  double x, y, w, h;
  box_rectangle(&x, &y, &w, &h);
  cairo_rectangle(cr, x, y, w, h);
  cairo_set_source_rgba(cr, 1, 1, 1, 0.5);
  cairo_fill_preserve(cr);
  cairo_set_source_rgba(cr, 1, 1, 1, 1);
  cairo_stroke(cr);
}

static void draw_annotation(cairo_t *ctx, struct annotation *ls) {
  if (ls->selected) {
    cairo_set_source_rgba(ctx, 1, 0, 0, 1);
  } else {
    cairo_set_source_rgba(ctx, 1, 1, 1, 1);
  }
  struct point *l = 0;
  switch (ls->tag) {
    case annotation_ray_in:  l = ls->u.ray_in.line; break;
    case annotation_ray_out: l = ls->u.ray_out.line; break;
    case annotation_text: {
      double x, y;
      param_to_screen(&x, &y, ls->u.text.x, ls->u.text.y);
      if (-G.width <= x && x < 2 * G.width && -G.height <= y && y < 2 * G.height) {
        cairo_select_font_face(ctx, "LMSans10", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
        cairo_set_font_size(ctx, 16.0);
        cairo_text_extents_t e;
        cairo_text_extents(ctx, ls->label, &e);
        cairo_move_to(ctx, x - e.width / 2.0, y + e.height / 2.0);
        cairo_text_path(ctx, ls->label);
        cairo_set_source_rgba(ctx, 0, 0, 0, 0.5);
        cairo_set_line_width(ctx, 4.0);
        cairo_stroke_preserve(ctx);
        if (ls->selected) {
          cairo_set_source_rgba(ctx, 1, 0, 0, 1);
        } else {
          cairo_set_source_rgba(ctx, 1, 1, 1, 1);
        }
        cairo_fill(ctx);
      }
      break;
    }
  }
  if (l) {
    cairo_set_line_width(ctx, 2.0);
    int first = 1;
    double x, y, oldx = 0, oldy = 0;
    int oldinbounds = 0;
    for (; l; l = l->next) {
      param_to_screen(&x, &y, l->x, l->y);
      int inbounds = 0 <= x && x < G.width && 0 <= y && y < G.height;
      if (oldinbounds) {
        cairo_line_to(ctx, x, y);
      } else {
        if (inbounds) {
          if (first) {
            cairo_move_to(ctx, x, y);
          } else {
            cairo_move_to(ctx, oldx, oldy);
            cairo_line_to(ctx, x, y);
          }
        } else {
          // segment out of bounds
        }
      }
      oldinbounds = inbounds;
      oldx = x;
      oldy = y;
      first = 0;
    }
    cairo_stroke(ctx);
  }
}

static void resize_image(int w, int h) {
  cairo_surface_destroy(G.surface);
  G.surface = 0;
  G.width = w;
  G.height = h;
  gtk_widget_set_size_request(G.da, G.width, G.height);
}

static gboolean configure_event_cb(GtkWidget *widget, GdkEventConfigure *event, gpointer data) {
  (void) event;
  (void) data;
  if (G.surface) {
    return TRUE;
  }
  G.surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, G.width, G.height);
  struct view *v = view_copy(G.view);
  start_render(v);
  gtk_widget_queue_draw(widget);
  return TRUE;
}

static gboolean draw_cb(GtkWidget *widget, cairo_t *cr, gpointer data) {
  (void) widget;
  (void) data;
  cairo_set_source_surface (cr, G.surface, 0, 0);
  cairo_paint (cr);
  if (G.box) {
    draw_box(cr);
  }
  for (int selected = 0; selected <= 1; ++selected) {
    for (struct annotation *ls = G.anno; ls; ls = ls->next) {
      if (ls->selected == selected) {
        draw_annotation(cr, ls);
      }
    }
  }
  return FALSE;
}

static void save_png(const char *filename) {
  cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, G.width, G.height);
  cairo_t *cr = cairo_create(surface);
  draw_cb(0, cr, 0);
  cairo_destroy(cr);
  cairo_surface_write_to_png(surface, filename);
  cairo_surface_destroy(surface);
}

static void close_window(void) {
  if (G.surface) {
    cairo_surface_destroy(G.surface);
  }
  gtk_main_quit();
}

static void save_clicked_cb(GtkToolButton *save, gpointer userdata) {
  (void) save;
  (void) userdata;
  GtkWidget *dialog = gtk_file_chooser_dialog_new("Save File", GTK_WINDOW(G.window), GTK_FILE_CHOOSER_ACTION_SAVE, "_Cancel", GTK_RESPONSE_CANCEL, "_Save", GTK_RESPONSE_ACCEPT, NULL);
  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    serialize(filename); // FIXME TODO handle errors
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}

static void save_image_clicked_cb(GtkToolButton *save_image, gpointer userdata) {
  (void) save_image;
  (void) userdata;
  GtkWidget *dialog = gtk_file_chooser_dialog_new("Save PNG", GTK_WINDOW(G.window), GTK_FILE_CHOOSER_ACTION_SAVE, "_Cancel", GTK_RESPONSE_CANCEL, "_Save", GTK_RESPONSE_ACCEPT, NULL);
  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    save_png(filename); // FIXME TODO handle errors
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}

static void load_clicked_cb(GtkToolButton *load, gpointer userdata) {
  (void) load;
  (void) userdata;
  GtkWidget *dialog = gtk_file_chooser_dialog_new("Load File", GTK_WINDOW(G.window), GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    deserialize(filename); // FIXME TODO handle errors
    g_free(filename);
  }
  gtk_widget_destroy(dialog);
}

static void resize_clicked_cb(GtkToolButton *resize, gpointer userdata) {
  (void) resize;
  (void) userdata;
  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  GtkWidget *dialog = gtk_dialog_new_with_buttons("Size", GTK_WINDOW(G.window), flags, "_Cancel", GTK_RESPONSE_CANCEL, "_Resize", GTK_RESPONSE_ACCEPT, NULL);
  GtkWidget *box = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
  GtkWidget *entryw = gtk_entry_new();
  GtkWidget *entryh = gtk_entry_new();
  char *labelw = malloc(100);
  snprintf(labelw, 99, "%d", G.width);
  char *labelh = malloc(100);
  snprintf(labelh, 99, "%d", G.height);
  gtk_entry_set_text(GTK_ENTRY(entryw), labelw);
  gtk_entry_set_text(GTK_ENTRY(entryh), labelh);
  gtk_container_add(GTK_CONTAINER(box), entryw);
  gtk_container_add(GTK_CONTAINER(box), entryh);
  gtk_widget_show_all(dialog);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    const char *textw = gtk_entry_get_text(GTK_ENTRY(entryw));
    int w = atoi(textw);
    const char *texth = gtk_entry_get_text(GTK_ENTRY(entryh));
    int h = atoi(texth);
    if (w > 0 && h > 0) {
      resize_image(w, h);
    }
  }
  free(labelw);
  free(labelh);
  gtk_widget_destroy(dialog);
}

static void home_clicked_cb(GtkToolButton *home, gpointer userdata) {
  (void) home;
  (void) userdata;
  struct view *v = view_new();
  mpfr_set_d(v->cx, -0.75, GMP_RNDN);
  mpfr_set_d(v->cy, 0.0, GMP_RNDN);
  mpfr_set_d(v->radius, 1.5, GMP_RNDN);
  start_render(v);
}

static void zoom_out_clicked_cb(GtkToolButton *zoom_out, gpointer userdata) {
  (void) zoom_out;
  (void) userdata;
  struct view *v = view_copy(G.view);
  mpfr_mul_d(v->radius, v->radius, 10.0, GMP_RNDN);
  start_render(v);
}

static gboolean zoom_button_press_event_cb (GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    G.box = TRUE;
    G.box_x1 = event->x;
    G.box_y1 = event->y;
    G.box_x2 = event->x;
    G.box_y2 = event->y;
    gtk_widget_queue_draw(widget);
  } else {
    G.box = FALSE;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static gboolean zoom_button_release_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    if (G.box) {
      if (G.view) {
        double x, y, w, h;
        box_rectangle(&x, &y, &w, &h);
        if (w > 0 && h > 0) {
          struct view *v = view_new();
          x = (x + w/2.0 - G.width / 2.0) / (G.height / 2.0);
          y = (G.height / 2.0 - (y + h/2.0)) / (G.height / 2.0);
          double r = (h/2.0) / (G.height / 2.0);
          mpfr_t clickx, clicky, logradius;
          mpfr_init2(clickx, 53);
          mpfr_init2(clicky, 53);
          mpfr_init2(logradius, 53);
          mpfr_set_d(clickx, x, GMP_RNDN);
          mpfr_set_d(clicky, y, GMP_RNDN);
          mpfr_mul(clickx, clickx, G.view->radius, GMP_RNDN);
          mpfr_mul(clicky, clicky, G.view->radius, GMP_RNDN);
          mpfr_mul_d(v->radius, G.view->radius, r, GMP_RNDN);
          mpfr_log2(logradius, v->radius, GMP_RNDN);
          int prec = fmax(53, 16 - mpfr_get_d(logradius, GMP_RNDN));
          mpfr_prec_round(v->cx, prec, GMP_RNDN);
          mpfr_prec_round(v->cy, prec, GMP_RNDN);
          mpfr_add(v->cx, G.view->cx, clickx, GMP_RNDN);
          mpfr_add(v->cy, G.view->cy, clicky, GMP_RNDN);
          mpfr_clear(clickx);
          mpfr_clear(clicky);
          mpfr_clear(logradius);
          start_render(v);
        }
      }
      G.box = FALSE;
      gtk_widget_queue_draw (widget);
    }
  }
  return TRUE;
}

static gboolean zoom_motion_notify_event_cb(GtkWidget *widget, GdkEventMotion *event, gpointer data) {
  (void) data;
  if (event->state & GDK_BUTTON1_MASK) {
    G.box_x2 = event->x;
    G.box_y2 = event->y;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static void zoom_toggled_cb(GtkToggleToolButton *zoom, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_toggle_tool_button_get_active(zoom);
  if (active) {
    G.box_aspect = G.width / (double) G.height;
    G.zoom_motion_notify_handler = g_signal_connect(G.da, "motion-notify-event", G_CALLBACK(zoom_motion_notify_event_cb), NULL);
    G.zoom_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(zoom_button_press_event_cb), NULL);
    G.zoom_button_release_handler = g_signal_connect(G.da, "button-release-event", G_CALLBACK(zoom_button_release_event_cb), NULL);
  } else {
    if (G.zoom_motion_notify_handler) {
      g_signal_handler_disconnect(G.da, G.zoom_motion_notify_handler);
    }
    if (G.zoom_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.zoom_button_press_handler);
    }
    if (G.zoom_button_release_handler) {
      g_signal_handler_disconnect(G.da, G.zoom_button_release_handler);
    }
    G.zoom_motion_notify_handler = 0;
    G.zoom_button_press_handler = 0;
    G.zoom_button_release_handler = 0;
  }
}

static void info_update(double x, double y) {
  int i = x, j = y;
  if (G.image && mandelbrot_image_in_bounds(G.image, i, j)) {
    mpfr_t cx, cy;
    mpfr_init2(cx, 53);
    mpfr_init2(cy, 53);
    screen_to_param(x, y, cx, cy);
    int dwell = 0, period = 0;
    struct mandelbrot_pixel *px = mandelbrot_image_peek(G.image, i, j);
    switch (px->tag) {
      case mandelbrot_pixel_exterior:
        dwell = px->u.exterior.final_n;
        period = px->u.exterior.final_p;
        break;
      case mandelbrot_pixel_interior:
        period = px->u.interior.final_p;
        break;
      default:
        break;
    }
    char *buf = calloc(1, 65536);
    mpfr_snprintf(buf, 65536,
      "\nINFO\n"
      "pixel: %d %d\n"
      "real: %Re\n"
      "imag: %Re\n"
      "dwell: %d\n"
      "period: %d",
      i, j, cx, cy, dwell, period);
    log_append(buf);
    free(buf);
    mpfr_clear(cx);
    mpfr_clear(cy);
  }
}

static gboolean info_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) widget;
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    info_update(event->x, event->y);
  }
  return TRUE;
}

static void info_toggled_cb(GtkToggleToolButton *info, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_toggle_tool_button_get_active(info);
  if (active) {
    G.info_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(info_button_press_event_cb), NULL);
  } else {
    if (G.info_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.info_button_press_handler);
    }
    G.info_button_press_handler = 0;
  }
}

static gboolean ray_out_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    start_ray_out(event->x, event->y);
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static void ray_out_toggled_cb(GtkToggleToolButton *ray_out, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_toggle_tool_button_get_active(ray_out);
  if (active) {
    G.ray_out_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(ray_out_button_press_event_cb), NULL);
  } else {
    if (G.ray_out_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.ray_out_button_press_handler);
    }
    G.ray_out_button_press_handler = 0;
  }
}

static void ray_in_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
  GtkTreeIter iter;
  GtkTreeModel *model;
  gpointer *thing;
  const char *label = "";
  if (gtk_tree_selection_get_selected(select, &model, &iter)) {
    gtk_tree_model_get(model, &iter, 1, &thing, -1);
    if (thing) {
      label = ((struct annotation *) thing)->label;
    }
  }
  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  GtkWidget *dialog = gtk_dialog_new_with_buttons("Ray In", GTK_WINDOW(G.window), flags, "_Cancel", GTK_RESPONSE_CANCEL, "_Ray In", GTK_RESPONSE_ACCEPT, NULL);
  GtkWidget *box = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
  GtkWidget *aentry = gtk_entry_new();
  GtkWidget *dentry = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(aentry), label);
  gtk_entry_set_text(GTK_ENTRY(dentry), "1000");
  gtk_container_add(GTK_CONTAINER(box), aentry);
  gtk_container_add(GTK_CONTAINER(box), dentry);
  gtk_widget_show_all(dialog);
  gint res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
    const char *atext = gtk_entry_get_text(GTK_ENTRY(aentry));
    const char *dtext = gtk_entry_get_text(GTK_ENTRY(dentry));
    struct mandelbrot_binary_angle *angle = mandelbrot_binary_angle_from_string(atext);
    int depth = atoi(dtext);
    if (angle && depth > 0) {
      start_ray_in(angle, depth);
    }
  }
  gtk_widget_destroy(dialog);
}

static void extend_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
  GtkTreeIter iter;
  GtkTreeModel *model;
  gpointer *thing;
  if (gtk_tree_selection_get_selected(select, &model, &iter)) {
    gtk_tree_model_get(model, &iter, 1, &thing, -1);
    if (thing) {
      struct annotation *anno = (struct annotation *) thing;
      if (anno->tag == annotation_ray_in) {
        GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
        GtkWidget *dialog = gtk_dialog_new_with_buttons("Extend", GTK_WINDOW(G.window), flags, "_Cancel", GTK_RESPONSE_CANCEL, "_Extend", GTK_RESPONSE_ACCEPT, NULL);
        GtkWidget *box = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
        GtkWidget *dentry = gtk_entry_new();
        gtk_entry_set_text(GTK_ENTRY(dentry), "1000");
        gtk_container_add(GTK_CONTAINER(box), dentry);
        gtk_widget_show_all(dialog);
        gint res = gtk_dialog_run(GTK_DIALOG(dialog));
        if (res == GTK_RESPONSE_ACCEPT) {
          const char *dtext = gtk_entry_get_text(GTK_ENTRY(dentry));
          int depth = atoi(dtext);
          if (depth > 0) {
            start_ray_in_extend(anno, depth);
          }
        }
        gtk_widget_destroy(dialog);
      }
    }
  }
}

struct nucleus_data {
  struct annotation *anno;
  mpfr_t r;
};

static gboolean nucleus_progress(gpointer userdata) {
  (void) userdata;
  if (! G.cancelled) {
    gtk_progress_bar_pulse(GTK_PROGRESS_BAR(G.bar));
  }
  return G_SOURCE_REMOVE;
}

static void nucleus_progress_thread(void) {
  gdk_threads_add_idle(nucleus_progress, 0);
}

static gboolean nucleus_thread_cancelled(gpointer user_data) {
  (void) user_data;
  G.dialog = 0;
  G.bar = 0;
  return G_SOURCE_REMOVE;
}

static gboolean nucleus_thread_done(gpointer user_data) {
  (void) user_data;
  gtk_widget_destroy(G.dialog);
  G.dialog = 0;
  G.bar = 0;
  gtk_widget_queue_draw(G.da);
  return G_SOURCE_REMOVE;
}

static gpointer nucleus_thread(gpointer data) {
  struct nucleus_data *d = data;
  nucleus_progress_thread();
  int period = mandelbrot_box_period(d->anno->u.text.x, d->anno->u.text.y, d->r, G.maximum_iterations);
  nucleus_progress_thread();
  d->anno->label = malloc(100);
  snprintf(d->anno->label, 99, "%d", period);
  if (! G.cancelled) {
    nucleus_progress_thread();
    mandelbrot_nucleus(d->anno->u.text.x, d->anno->u.text.y, d->anno->u.text.x, d->anno->u.text.y, period, 64);
    nucleus_progress_thread();
  }
  if (! G.cancelled) {
    add_annotation_thread(d->anno);
    mpfr_clear(d->r);
    free(d);
    gdk_threads_add_idle(nucleus_thread_done, 0);
  } else {
    delete_annotation(d->anno);
    mpfr_clear(d->r);
    free(d);
    gdk_threads_add_idle(nucleus_thread_cancelled, 0);
  }
  return 0;
}

static void start_nucleus(mpfr_t x, mpfr_t y, mpfr_t r) {
  struct nucleus_data *d = malloc(sizeof(struct nucleus_data));
  d->anno = malloc(sizeof(struct annotation));
  d->anno->next = 0;
  d->anno->selected = 0;
  d->anno->label = 0;
  d->anno->tag = annotation_text;
  mpfr_init2(d->anno->u.text.x, mpfr_get_prec(x));
  mpfr_init2(d->anno->u.text.y, mpfr_get_prec(y));
  mpfr_init2(d->r, 53);
  mpfr_set(d->anno->u.text.x, x, GMP_RNDN);
  mpfr_set(d->anno->u.text.y, y, GMP_RNDN);
  mpfr_set(d->r, r, GMP_RNDN);

  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  G.dialog = gtk_dialog_new_with_buttons("Progress", GTK_WINDOW(G.window), flags, NULL, NULL);
  GtkWidget *box = gtk_dialog_get_content_area(GTK_DIALOG(G.dialog));
  G.bar = gtk_progress_bar_new();
  gtk_container_add(GTK_CONTAINER(box), G.bar);

  G.cancelled = 0;
  G.progress = 0;

  GThread *thread = g_thread_new("nucleus", nucleus_thread, d);
  gtk_widget_show_all(G.dialog);
  gtk_dialog_run(GTK_DIALOG(G.dialog));

  G.cancelled = 1;
  g_thread_join(thread);
  if (G.dialog) {
    gtk_widget_destroy(G.dialog);
  }
}

static gboolean nucleus_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    G.box = TRUE;
    G.box_x1 = event->x;
    G.box_y1 = event->y;
    G.box_x2 = event->x;
    G.box_y2 = event->y;
    gtk_widget_queue_draw(widget);
  } else {
    G.box = FALSE;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static gboolean nucleus_button_release_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    if (G.box) {
      if (G.view) {
        double x, y, w, h;
        box_rectangle(&x, &y, &w, &h);
        if (w > 0 && h > 0) {
          x = (x + w/2.0 - G.width / 2.0) / (G.height / 2.0);
          y = (G.height / 2.0 - (y + h/2.0)) / (G.height / 2.0);
          double r = (h/2.0) / (G.height / 2.0);
          mpfr_t clickx, clicky, clickr, logradius;
          mpfr_init2(clickx, 53);
          mpfr_init2(clicky, 53);
          mpfr_init2(clickr, 53);
          mpfr_init2(logradius, 53);
          mpfr_set_d(clickx, x, GMP_RNDN);
          mpfr_set_d(clicky, y, GMP_RNDN);
          mpfr_mul(clickx, clickx, G.view->radius, GMP_RNDN);
          mpfr_mul(clicky, clicky, G.view->radius, GMP_RNDN);
          mpfr_mul_d(clickr, G.view->radius, r, GMP_RNDN);
          mpfr_log2(logradius, clickr, GMP_RNDN);
          int prec = fmax(53, 16 - mpfr_get_d(logradius, GMP_RNDN));
          mpfr_prec_round(clickx, prec, GMP_RNDN);
          mpfr_prec_round(clicky, prec, GMP_RNDN);
          mpfr_add(clickx, G.view->cx, clickx, GMP_RNDN);
          mpfr_add(clicky, G.view->cy, clicky, GMP_RNDN);
          mpfr_clear(logradius);
          start_nucleus(clickx, clicky, clickr);
          mpfr_clear(clickx);
          mpfr_clear(clicky);
          mpfr_clear(clickr);
        }
      }
      G.box = FALSE;
      gtk_widget_queue_draw(widget);
    }
  }
  return TRUE;
}

static gboolean nucleus_motion_notify_event_cb(GtkWidget *widget, GdkEventMotion *event, gpointer data) {
  (void) data;
  if (event->state & GDK_BUTTON1_MASK) {
    G.box_x2 = event->x;
    G.box_y2 = event->y;
    gtk_widget_queue_draw(widget);
  }
  return TRUE;
}

static void nucleus_toggled_cb(GtkToggleToolButton *nucleus, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_toggle_tool_button_get_active(nucleus);
  if (active) {
    G.box_aspect = 1.0;
    G.nucleus_motion_notify_handler = g_signal_connect(G.da, "motion-notify-event", G_CALLBACK(nucleus_motion_notify_event_cb), NULL);
    G.nucleus_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(nucleus_button_press_event_cb), NULL);
    G.nucleus_button_release_handler = g_signal_connect(G.da, "button-release-event", G_CALLBACK(nucleus_button_release_event_cb), NULL);
  } else {
    if (G.nucleus_motion_notify_handler) {
      g_signal_handler_disconnect(G.da, G.nucleus_motion_notify_handler);
    }
    if (G.nucleus_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.nucleus_button_press_handler);
    }
    if (G.nucleus_button_release_handler) {
      g_signal_handler_disconnect(G.da, G.nucleus_button_release_handler);
    }
    G.nucleus_motion_notify_handler = 0;
    G.nucleus_button_press_handler = 0;
    G.nucleus_button_release_handler = 0;
  }
}

static gboolean bond_button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data) {
  (void) data;
  if (event->button == GDK_BUTTON_PRIMARY) {
    int i = event->x;
    int j = event->y;
    if (mandelbrot_image_in_bounds(G.image, i, j)) {
      struct mandelbrot_pixel *px = mandelbrot_image_peek(G.image, i, j);
      if (px->tag == mandelbrot_pixel_interior) {
        int period = px->u.interior.final_p;
#define VARS cx, cy, rootx, rooty, parentx, parenty, epsilon
        mpfr_t VARS;
        mpfr_inits2(mpfr_get_prec(G.view->cx), VARS, (mpfr_ptr) 0);
        mpfr_set(epsilon, G.view->pixel_spacing, GMP_RNDN);
        mpfr_mul_2si(epsilon, epsilon, -8, GMP_RNDN);
        screen_to_param(event->x, event->y, cx, cy);
        mandelbrot_nucleus(cx, cy, cx, cy, period, 64);
        mpq_t angle;
        mpq_init(angle);
        mandelbrot_parent(angle, rootx, rooty, parentx, parenty, cx, cy, period, 64, epsilon, 64);
        if (mpz_sgn(mpq_numref(angle))) {
          char buffer[100];
          mpfr_snprintf(buffer, 99, "%Qd", angle);
          struct annotation *anno = malloc(sizeof(struct annotation));
          anno->next = 0;
          anno->selected = 0;
          anno->label = malloc(strlen(buffer) + 1);
          anno->label[0] = 0;
          strncat(anno->label, buffer, strlen(buffer));
          anno->tag = annotation_text;
          mpfr_init2(anno->u.text.x, mpfr_get_prec(rootx));
          mpfr_init2(anno->u.text.y, mpfr_get_prec(rooty));
          mpfr_set(anno->u.text.x, rootx, GMP_RNDN);
          mpfr_set(anno->u.text.y, rooty, GMP_RNDN);
          add_annotation(anno);
          gtk_widget_queue_draw(widget);
        }
        mpq_clear(angle);
        mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
      }
    }
  }
  return TRUE;
}

static void bond_toggled_cb(GtkToggleToolButton *bond, gpointer userdata) {
  (void) userdata;
  gboolean active = gtk_toggle_tool_button_get_active(bond);
  if (active) {
    G.bond_button_press_handler = g_signal_connect(G.da, "button-press-event", G_CALLBACK(bond_button_press_event_cb), NULL);
  } else {
    if (G.bond_button_press_handler) {
      g_signal_handler_disconnect(G.da, G.bond_button_press_handler);
    }
    G.bond_button_press_handler = 0;
  }
}

static void annotree_selection_changed_cb(GtkTreeSelection *selection, gpointer data) {
  (void) data;
  GtkTreeIter iter;
  GtkTreeModel *model;
  gpointer *thing;
  if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
    gtk_tree_model_get(model, &iter, 1, &thing, -1);
    struct annotation *ls;
    for (ls = G.anno; ls; ls = ls->next) {
      ls->selected = 0;
    }
    ls = (struct annotation *) thing;
    if (ls) {
      ls->selected = 1;
    }
  }
  gtk_widget_queue_draw(G.da);
}

static void delete_clicked_cb(GtkToolButton *toolbutton, gpointer user_data) {
  (void) toolbutton;
  (void) user_data;
  GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
  GtkTreeIter iter;
  GtkTreeModel *model;
  gpointer *thing;
  if (gtk_tree_selection_get_selected(select, &model, &iter)) {
    gtk_tree_model_get(model, &iter, 1, &thing, -1);
    if (thing) {
      delete_annotation((struct annotation *) thing);
      gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
    }
  }
}

static int serialize(const char *filename) {
  FILE *out = fopen(filename, "wb");
  if (out) {
    fprintf(out, "size %d %d\n", G.width, G.height);
    mpfr_fprintf(out, "view %Pd %Re %Re %Re\n", mpfr_get_prec(G.view->cx), G.view->cx, G.view->cy, G.view->radius);
    for (struct annotation *anno = G.anno; anno; anno = anno->next) {
      switch (anno->tag) {
        case annotation_ray_out: {
          struct point *p = anno->u.ray_out.line;
          while (p->next) {
            p = p->next;
          }
          mpfr_fprintf(out, "ray_out %Pd %Re %Re\n", mpfr_get_prec(p->x), p->x, p->y);
          break;
        }
        case annotation_ray_in: {
          char *s = mandelbrot_binary_angle_to_string(anno->u.ray_in.angle);
          fprintf(out, "ray_in %d %s\n", anno->u.ray_in.depth, s);
          free(s);
          break;
        }
        case annotation_text: {
          mpfr_fprintf(out, "text %Pd %Re %Re %s\n", mpfr_get_prec(anno->u.text.x), anno->u.text.x, anno->u.text.y, anno->label);
          break;
        }
      }
    }
    fclose(out);
    return 0;
  } else {
    return 1;
  }
}

static int deserialize(const char *filename) {
  FILE *in = fopen(filename, "rb");
  if (in) {
    while (G.anno) {
      GtkTreeIter iter;
      gpointer *thing;
      gtk_tree_model_get_iter_first(GTK_TREE_MODEL(G.annostore), &iter);
      if (gtk_tree_model_iter_next(GTK_TREE_MODEL(G.annostore), &iter)) {
        gtk_tree_model_get(GTK_TREE_MODEL(G.annostore), &iter, 1, &thing, -1);
        if (thing) {
          delete_annotation((struct annotation *) thing);
          gtk_tree_store_remove(G.annostore, &iter);
        }
      }
    }
    char *line = 0;
    size_t linelen = 0;
    ssize_t readlen = 0;
    while (-1 != (readlen = getline(&line, &linelen, in))) {
      if (readlen && line[readlen-1] == '\n') {
        line[readlen - 1] = 0;
      }
      if (0 == strncmp(line, "size ", 5)) {
        int w = 0, h = 0;
        sscanf(line + 5, "%d %d\n", &w, &h);
        // resize_image(w, h); // FIXME TODO make this work...
      } else if (0 == strncmp(line, "view ", 5)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        char *rs = malloc(readlen);
        sscanf(line + 5, "%d %s %s %s", &p, xs, ys, rs);
        struct view *v = view_new();
        mpfr_set_prec(v->cx, p);
        mpfr_set_prec(v->cy, p);
        set_result = mpfr_set_str(v->cx, xs, 0, GMP_RNDN);
        set_result = set_result + mpfr_set_str(v->cy, ys, 0, GMP_RNDN);
        set_result = set_result + mpfr_set_str(v->radius, rs, 0, GMP_RNDN);
        free(xs);
        free(ys);
        free(rs);
        if (set_result < 0) { printf("view line not valid, check chars\n"); return 1; }
        start_render(v);
      } else if (0 == strncmp(line, "ray_out ", 8)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        sscanf(line + 8, "%d %s %s", &p, xs, ys);
        mpfr_t cx, cy;
        mpfr_init2(cx, p);
        mpfr_init2(cy, p);
        set_result = mpfr_set_str(cx, xs, 0, GMP_RNDN);
        set_result = set_result + mpfr_set_str(cy, ys, 0, GMP_RNDN);
        free(xs);
        free(ys);
        double x = 0, y = 0;
        param_to_screen(&x, &y, cx, cy);
        mpfr_clear(cx);
        mpfr_clear(cy);
        if (set_result < 0) { printf("ray_out line not valid, check chars\n"); return 1; }
        start_ray_out(x, y);
      } else if (0 == strncmp(line, "ray_in ", 7)) {
        int depth = 1000;
        char *as = malloc(readlen);
        sscanf(line + 7, "%d %s", &depth, as);
        struct mandelbrot_binary_angle *angle = mandelbrot_binary_angle_from_string(as);
        struct mandelbrot_binary_angle *angle2 = mandelbrot_binary_angle_canonicalize(angle);
        mandelbrot_binary_angle_delete(angle);
        start_ray_in(angle2, depth);
        free(as);
      } else if (0 == strncmp(line, "text ", 5)) {
        int p = 53;
        int set_result;
        char *xs = malloc(readlen);
        char *ys = malloc(readlen);
        char *ss = malloc(readlen);
        sscanf(line + 5, "%d %s %s %s", &p, xs, ys, ss);
        struct annotation *anno = calloc(1, sizeof(struct annotation));
        anno->tag = annotation_text;
        anno->label = ss;
        mpfr_init2(anno->u.text.x, p);
        mpfr_init2(anno->u.text.y, p);
        set_result = mpfr_set_str(anno->u.text.x, xs, 0, GMP_RNDN);
        set_result = set_result + mpfr_set_str(anno->u.text.y, ys, 0, GMP_RNDN);
        free(xs);
        free(ys);
        if (set_result < 0) {
          free(ss);
          mpfr_clear(anno->u.text.x);
          mpfr_clear(anno->u.text.y);
          printf("text line not valid, check chars \n");
          return 1;
        }
        add_annotation(anno);
      }
    }
    free(line);
    fclose(in);
    return 0;
  } else {
    return 1;
  }
}

int main(int argc, char **argv) {
  memset(&G, 0, sizeof(G));
  G.width = 640;
  G.height = 360;
  mpfr_init2(G.escape_radius2, 53);
  mpfr_set_d(G.escape_radius2, 512.0 * 512.0, GMP_RNDN);
  G.maximum_iterations = 65536;
  G.view = view_new();
  mpfr_set_d(G.view->cx, -0.75, GMP_RNDN);
  mpfr_set_d(G.view->cy, 0.0, GMP_RNDN);
  mpfr_set_d(G.view->radius, 1.5, GMP_RNDN);

  gtk_disable_setlocale(); // FIXME TODO find a better way for safe (de)serialisation
  gtk_init (&argc, &argv);

  G.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(G.window), "Mandelbrot");
  gtk_window_set_resizable(GTK_WINDOW(G.window), FALSE);

  g_signal_connect(G.window, "destroy", G_CALLBACK(close_window), NULL);

  GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_container_add(GTK_CONTAINER(G.window), hbox);
  gtk_container_add(GTK_CONTAINER(hbox), vbox);

  G.da = gtk_drawing_area_new();
  gtk_widget_set_size_request(G.da, G.width, G.height);
  g_signal_connect(G.da, "draw", G_CALLBACK(draw_cb), NULL);
  g_signal_connect(G.da,"configure-event", G_CALLBACK(configure_event_cb), NULL);
  gtk_widget_set_events(G.da, gtk_widget_get_events(G.da) | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK);

  GtkWidget *tbar = gtk_toolbar_new();
  gtk_toolbar_set_style(GTK_TOOLBAR(tbar), GTK_TOOLBAR_TEXT);
  GtkToolItem *save = gtk_tool_button_new(NULL, "Save");
  g_signal_connect(save, "clicked", G_CALLBACK(save_clicked_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), save, -1);
  GtkToolItem *save_image = gtk_tool_button_new(NULL, "Save Image");
  g_signal_connect(save_image, "clicked", G_CALLBACK(save_image_clicked_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), save_image, -1);
  GtkToolItem *load = gtk_tool_button_new(NULL, "Load");
  g_signal_connect(load, "clicked", G_CALLBACK(load_clicked_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), load, -1);
  GtkToolItem *resize = gtk_tool_button_new(NULL, "Resize");
  g_signal_connect(resize, "clicked", G_CALLBACK(resize_clicked_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), resize, -1);
  GtkToolItem *home = gtk_tool_button_new(NULL, "Home");
  g_signal_connect(home, "clicked", G_CALLBACK(home_clicked_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), home, -1);
  GtkToolItem *zoom_out = gtk_tool_button_new(NULL, "Zoom Out");
  g_signal_connect(zoom_out, "clicked", G_CALLBACK(zoom_out_clicked_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), zoom_out, -1);
  GtkToolItem *zoom = gtk_radio_tool_button_new_from_widget(NULL);
  gtk_tool_button_set_label(GTK_TOOL_BUTTON(zoom), "Zoom");
  g_signal_connect(zoom, "toggled", G_CALLBACK(zoom_toggled_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), zoom, -1);
  GtkToolItem *info = gtk_radio_tool_button_new_from_widget(GTK_RADIO_TOOL_BUTTON(zoom));
  gtk_tool_button_set_label(GTK_TOOL_BUTTON(info), "Info");
  g_signal_connect(info, "toggled", G_CALLBACK(info_toggled_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), info, -1);
  GtkToolItem *ray_out = gtk_radio_tool_button_new_from_widget(GTK_RADIO_TOOL_BUTTON(zoom));
  gtk_tool_button_set_label(GTK_TOOL_BUTTON(ray_out), "Ray Out");
  g_signal_connect(ray_out, "toggled", G_CALLBACK(ray_out_toggled_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), ray_out, -1);
  GtkToolItem *ray_in = gtk_tool_button_new(NULL, "Ray In");
  g_signal_connect(ray_in, "clicked", G_CALLBACK(ray_in_clicked_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), ray_in, -1);
  GtkToolItem *nucleus = gtk_radio_tool_button_new_from_widget(GTK_RADIO_TOOL_BUTTON(zoom));
  gtk_tool_button_set_label(GTK_TOOL_BUTTON(nucleus), "Nucleus");
  g_signal_connect(nucleus, "toggled", G_CALLBACK(nucleus_toggled_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), nucleus, -1);
  GtkToolItem *bond = gtk_radio_tool_button_new_from_widget(GTK_RADIO_TOOL_BUTTON(zoom));
  gtk_tool_button_set_label(GTK_TOOL_BUTTON(bond), "Bond");
  g_signal_connect(bond, "toggled", G_CALLBACK(bond_toggled_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar), bond, -1);

  G.log = gtk_text_view_new();
  gtk_text_view_set_editable(GTK_TEXT_VIEW(G.log), FALSE);
  gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(G.log), FALSE);
  gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(G.log), GTK_WRAP_CHAR);
  GtkWidget *scroll = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_size_request(scroll, G.width, G.height / 3);
  gtk_container_add(GTK_CONTAINER(scroll), G.log);

  gtk_container_add(GTK_CONTAINER(vbox), tbar);
  gtk_container_add(GTK_CONTAINER(vbox), G.da);
  gtk_container_add(GTK_CONTAINER(vbox), scroll);

  GtkWidget *vbox2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  G.annostore = gtk_tree_store_new(2, G_TYPE_STRING, G_TYPE_POINTER);
  G.annotree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(G.annostore));
  g_object_unref(G_OBJECT(G.annostore));
  GtkTreeIter iter;
  gtk_tree_store_append(G.annostore, &iter, NULL);
  gtk_tree_store_set(G.annostore, &iter, 0, "(none)", 1, NULL, -1);
  GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
  GtkTreeViewColumn *column = gtk_tree_view_column_new_with_attributes("Annotations", renderer, "text", 0, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(G.annotree), column);
  GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(G.annotree));
  gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);
  g_signal_connect(select, "changed", G_CALLBACK(annotree_selection_changed_cb), NULL);
  scroll = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_size_request(scroll, G.width / 2, G.height * 4 / 3);
  gtk_container_add(GTK_CONTAINER(scroll), G.annotree);
  gtk_container_add(GTK_CONTAINER(vbox2), scroll);
  GtkWidget *tbar2 = gtk_toolbar_new();
  gtk_toolbar_set_style(GTK_TOOLBAR(tbar2), GTK_TOOLBAR_TEXT);
  GtkToolItem *delete = gtk_tool_button_new(NULL, "Delete");
  g_signal_connect(delete, "clicked", G_CALLBACK(delete_clicked_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar2), delete, -1);
  GtkToolItem *extend = gtk_tool_button_new(NULL, "Extend");
  g_signal_connect(extend, "clicked", G_CALLBACK(extend_clicked_cb), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(tbar2), extend, -1);
  gtk_container_add(GTK_CONTAINER(vbox2), tbar2);
  gtk_container_add(GTK_CONTAINER(hbox), vbox2);

  gtk_widget_show_all(G.window);
  gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(zoom), TRUE);
  zoom_toggled_cb(GTK_TOGGLE_TOOL_BUTTON(zoom), 0);

  gtk_main();
  return 0;
}
