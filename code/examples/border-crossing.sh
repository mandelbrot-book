#!/bin/bash
# test rays crossing image borders in both directions
view="0.275336142511115 6.75982538465039e-3 0.666e-5"
./render $view && ./colour > out.ppm && ./annotate out.ppm <<EOF
rgba 1 1 1 1
line ./ray_out 53 0.275336142511115 6.75982538465039e-3 | ./rescale 53 53 $view 0
EOF
