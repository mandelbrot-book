#!/bin/bash
# test boundary tracing
view="-0.75 0 1.5"
./render $view && ./colour > out.ppm && ./annotate out.ppm <<EOF
rgba 1 1 1 1
line ./bin/mandelbrot_boundary 53  0 0 1 100 1000 | ./rescale 53 53 $view 0
line ./bin/mandelbrot_boundary 53 -1 0 2 100 1000 | ./rescale 53 53 $view 0
EOF
