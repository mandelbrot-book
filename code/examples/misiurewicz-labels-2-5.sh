#!/bin/bash
if [[ -d misiurewicz-database ]]
then
(
  echo '#!/bin/bash' &&
  echo 'view="-5.7e-01 6.1469123628153433e-01 0.125"' &&
  echo './render $view 512 out 960 1080 &&' &&
  echo './colour out 1 > out.ppm &&' &&
  echo './force-layout <<EOF | ./annotate out.ppm' &&
  echo 'rgba 0 0 0 1' &&
  echo -e "^5.* \\.01001\n^5.* \\.01010" |
  grep -hf - misiurewicz-database/misiurewicz_*_*.txt |
  sed "s/^5 [^ ]* [^ ]* [^ ]* \([^ ]*\) .*$/\1/g" |
  grep -Ff - misiurewicz-database/landing_*_*.txt |
  sed 's@^misiurewicz-database/landing_\(.*\)_\(.*\).txt:\([^ ]*\) \([^ ]*\) .*$@text `echo "\3 \4" | ./rescale 53 53 $view 0` \1p\2@g' &&
  echo 'EOF'
) | bash
else
  echo "run ./examples/misiurewicz-database.sh first"
  exit 1
fi
