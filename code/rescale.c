#include <stdio.h>
#include <stdlib.h>
#include <mpfr.h>

void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s inbits outbits subre subim divre divim\n"
    "expects space-separated complex numbers on stdin\n"
    "outputs space-separated complex numbers on stdout\n"
    "example: echo \"0 0\" | %s 53 24 0 0 1 0\n",
    progname, progname);
}

int main(int argc, char **argv) {
  // parse args
  if (argc != 7) { usage(argv[0]); return 1; }
  int ibits = atoi(argv[1]);
  int obits = atoi(argv[2]);
  int bits = ibits > obits ? ibits : obits;
  mpfr_t subre; mpfr_init2(subre, bits); mpfr_set_str(subre, argv[3], 10, GMP_RNDN);
  mpfr_t subim; mpfr_init2(subim, bits); mpfr_set_str(subim, argv[4], 10, GMP_RNDN);
  mpfr_t divre; mpfr_init2(divre, bits); mpfr_set_str(divre, argv[5], 10, GMP_RNDN);
  mpfr_t divim; mpfr_init2(divim, bits); mpfr_set_str(divim, argv[6], 10, GMP_RNDN);
  // mul := 1 / div
  mpfr_t mulre; mpfr_init2(mulre, bits); mpfr_set(mulre, divre, GMP_RNDN);
  mpfr_t mulim; mpfr_init2(mulim, bits); mpfr_neg(mulim, divim, GMP_RNDN);
  mpfr_sqr(divre, divre, GMP_RNDN);
  mpfr_sqr(divim, divim, GMP_RNDN);
  mpfr_add(divre, divre, divim, GMP_RNDN);
  mpfr_div(mulre, mulre, divre, GMP_RNDN);
  mpfr_div(mulim, mulim, divre, GMP_RNDN);
  // variables  
  mpfr_t zre; mpfr_init2(zre, bits);
  mpfr_t zim; mpfr_init2(zim, bits);
  mpfr_t wre; mpfr_init2(wre, bits);
  mpfr_t wim; mpfr_init2(wim, bits);
  mpfr_t tmp; mpfr_init2(tmp, bits);
  mpfr_t ire; mpfr_init2(ire, ibits);
  mpfr_t iim; mpfr_init2(iim, ibits);
  mpfr_t ore; mpfr_init2(ore, obits);
  mpfr_t oim; mpfr_init2(oim, obits);
  // i <- stdin
  while (mpfr_inp_str(ire, 0, 10, GMP_RNDN) && mpfr_inp_str(iim, 0, 10, GMP_RNDN)) {
    // z := i - sub
    mpfr_sub(zre, ire, subre, GMP_RNDN);
    mpfr_sub(zim, iim, subim, GMP_RNDN);
    // w := z * mul
    mpfr_mul(wre, zre, mulre, GMP_RNDN);
    mpfr_mul(tmp, zim, mulim, GMP_RNDN);
    mpfr_sub(wre, wre, tmp, GMP_RNDN);
    mpfr_mul(wim, zre, mulim, GMP_RNDN);
    mpfr_mul(tmp, zim, mulre, GMP_RNDN);
    mpfr_add(wim, wim, tmp, GMP_RNDN);
    // o := w
    mpfr_set(ore, wre, GMP_RNDN);
    mpfr_set(oim, wim, GMP_RNDN);
    // stdout <- o
    mpfr_out_str(0, 10, 0, ore, GMP_RNDN);
    putchar(' ');
    mpfr_out_str(0, 10, 0, oim, GMP_RNDN);
    putchar('\n');
  }
  // cleanup
  mpfr_clears(subre, subim, divre, divim, mulre, mulim, zre, zim, wre, wim, tmp, ire, iim, ore, oim, (mpfr_ptr) 0);
  return 0;
}
