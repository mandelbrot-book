#include "mandelbrot.h"

#define width 1280
#define height 720
#define maximum_iterations 12

complex float z[height][width];

int channel(float c) {
  return fminf(fmaxf(roundf(255 * c), 0), 255);
}

int main() {
  memset(z, 0, sizeof(z[0][0]) * height * width);
  for (int n = 0; n <= maximum_iterations; ++n) {
    fprintf(stdout, "P5\n%d %d\n255\n", width, height);
    for (int j = 0; j < height; ++j) {
      for (int i = 0; i < width; ++i) {
        int k = channel(log2f(cabsf(z[j][i])/2.0 + 1.0f));
        fputc(k, stdout);
      }
    }
    if (n < maximum_iterations) {
      #pragma omp parallel for
      for (int j = 0; j < height; ++j) {
        for (int i = 0; i < width; ++i) {
          complex float c = coordinatef(i, j, width, height, -0.75f, 1.25f);
          z[j][i] = z[j][i] * z[j][i] + c;
        }
      }      
    }
  }
  return 0;
}
