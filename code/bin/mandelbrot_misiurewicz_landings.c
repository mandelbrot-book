#include <assert.h>
#include <complex.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include "mandelbrot.h"

struct mandelbrot_binary_angle *angle(int preperiod, int period, uint64_t digits) {
  assert(1 <= period && period <= 64);
  assert(0 <= preperiod && preperiod <= 64);
  assert(period + preperiod <= 64);
  uint64_t pre = digits >> period;
  uint64_t per = digits - (pre << period);
  char s[128];
  s[0] = '.';
  int k = 1;
  for (int i = 1; i <= preperiod; ++i) {
    s[k++] = '0' + !!(pre & ((uint64_t) 1 << (preperiod - i)));
  }
  s[k++] = '(';
  for (int i = 1; i <= period; ++i) {
    s[k++] = '0' + !!(per & ((uint64_t) 1 << (period - i)));
  }
  s[k++] = ')';
  s[k] = 0;
  return mandelbrot_binary_angle_from_string(s);
}

int main(int argc, char **argv) {
  int min_bits = 2;
  int max_bits = 16;
  if (argc > 1) {
    min_bits = fmax(atoi(argv[1]), 2);
  }
  if (argc > 2) {
    max_bits = fmin(atoi(argv[2]), 63);
  }
  for (int bits = min_bits; bits <= max_bits; ++bits) {
    for (int preperiod = 1; preperiod < bits; ++preperiod) {
      int period = bits - preperiod;
      char filename[1024];
      snprintf(filename, 1000, "landing_%d_%d.txt", preperiod, period);
      printf("%s\n", filename);
      FILE *out = fopen(filename, "wb");
      if (! out) {
        return 1;
      }
      #pragma omp parallel for
      for (uint64_t digits = 0; digits < (uint64_t) 1 << bits; ++digits) {
        struct mandelbrot_binary_angle *t = angle(preperiod, period, digits);
        if (mandelbrot_binary_angle_is_canonical(t)) {
          mpq_t q;
          mpq_init(q);
          mandelbrot_binary_angle_to_rational(q, t);
          mpq_canonicalize(q);
          struct mandelbrot_external_ray_in_native *ray = mandelbrot_external_ray_in_native_new(q);
          complex double c_old = mandelbrot_external_ray_in_native_get(ray);
          for (int i = 0; i < 10000000; ++i) {
            mandelbrot_external_ray_in_native_step(ray);
            complex double c = mandelbrot_external_ray_in_native_get(ray);
            if (! (c != c_old)) {
              break;
            }
            c_old = c;
          }
          mandelbrot_external_ray_in_native_delete(ray);
          mpq_clear(q);
          char *s = mandelbrot_binary_angle_to_string(t);
          #pragma omp critical
          {
            fprintf(out, "%.18e %.18e %s\n", creal(c_old), cimag(c_old), s);
          }
          free(s);
        }
        mandelbrot_binary_angle_delete(t);
      }
      fclose(out);
    }
  }
  return 0;
}
