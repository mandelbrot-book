#include <stdio.h>
#include <mpfr.h>
#include "mandelbrot_render.h"

static int progresscb(void *userdata, double progress) {
  (void) userdata;
  fprintf(stderr, "%7.2f%%\r", 100 * progress);
  return 1; // keep going
}

int main(int argc, char **argv) {
  int retval = 1;
  int width = 1280;
  int height = 720;
  int maximum_iterations = 4096;
  long double escape_radius = 512;
  mpfr_t radius;
  mpfr_init2(radius, 53);
  mpfr_t cre;
  mpfr_t cim;
  if (argc > 3) {
    mpfr_set_str(radius, argv[3], 10, GMP_RNDN);
  } else {
    mpfr_set_si(radius, 2, GMP_RNDN);
  }
  if (argc > 7) {
    width = atoi(argv[6]);
    height = atoi(argv[7]);
  }
  int float_type = 1;
  mpfr_t pixel_spacing;
  mpfr_init2(pixel_spacing, 53);
  mpfr_div_d(pixel_spacing, radius, height / 2.0, GMP_RNDN);
  mpfr_t pixel_spacing_log;
  mpfr_init2(pixel_spacing_log, 53);
  mpfr_log2(pixel_spacing_log, pixel_spacing, GMP_RNDN);
  int pixel_spacing_bits = -mpfr_get_d(pixel_spacing_log, GMP_RNDN);
  int interior = 1;
  if (argc > 9) {
    interior = atoi(argv[9]);
  }
  if (interior) {
    if (pixel_spacing_bits > 50/2) {
      float_type = 2;
    }
    if (pixel_spacing_bits > 60/2) {
      float_type = 3;
    }
  } else {
    if (pixel_spacing_bits > 50) {
      float_type = 2;
    }
    if (pixel_spacing_bits > 60) {
      float_type = 3;
    }
  }
  mpfr_init2(cre, pixel_spacing_bits + 8);
  mpfr_init2(cim, pixel_spacing_bits + 8);
  if (argc > 3) {
    mpfr_set_str(cre, argv[1], 10, GMP_RNDN);
    mpfr_set_str(cim, argv[2], 10, GMP_RNDN);
  } else {
    mpfr_set_d(cre, -0.75, GMP_RNDN);
    mpfr_set_d(cim, 0.0, GMP_RNDN);
  }
  if (argc > 4) {
    escape_radius = strtold(argv[4], 0);
  }
  mpfr_t escape_radius2;
  mpfr_init2(escape_radius2, pixel_spacing_bits + 8);
  mpfr_set_d(escape_radius2, escape_radius * escape_radius, GMP_RNDN);
  char *stem = "out";
  if (argc > 5) {
    stem = argv[5];
  }
  if (argc > 8) {
    maximum_iterations = atoi(argv[8]);
  }
  struct mandelbrot_image *img = mandelbrot_image_new(width, height);
  if (img) {
    switch (float_type) {
      case 0:
        printf("%s using float\n", argv[0]);
        mandelbrot_renderf(img, mpfr_get_d(cre, GMP_RNDN) + I * mpfr_get_d(cim, GMP_RNDN), mpfr_get_d(radius, GMP_RNDN), maximum_iterations, mpfr_get_d(escape_radius2, GMP_RNDN), mpfr_get_d(pixel_spacing, GMP_RNDN), interior, 0, progresscb);
        break;
      case 1:
        printf("%s using double\n", argv[0]);
        mandelbrot_render(img, mpfr_get_d(cre, GMP_RNDN) + I * mpfr_get_d(cim, GMP_RNDN), mpfr_get_d(radius, GMP_RNDN), maximum_iterations, mpfr_get_d(escape_radius2, GMP_RNDN), mpfr_get_d(pixel_spacing, GMP_RNDN), interior, 0, progresscb);
        break;
      case 2:
        printf("%s using long double\n", argv[0]);
        mandelbrot_renderl(img, mpfr_get_ld(cre, GMP_RNDN) + I * mpfr_get_ld(cim, GMP_RNDN), mpfr_get_ld(radius, GMP_RNDN), maximum_iterations, mpfr_get_ld(escape_radius2, GMP_RNDN), mpfr_get_ld(pixel_spacing, GMP_RNDN), interior, 0, progresscb);
        break;
      case 3:
        printf("%s using MPFR<%d>\n", argv[0], pixel_spacing_bits + 8);
        mandelbrot_render_mpfr(img, cre, cim, radius, maximum_iterations, escape_radius2, pixel_spacing, interior, 0, progresscb);
        break;
    }
    int err = mandelbrot_image_save(img, stem, escape_radius);
    mandelbrot_image_free(img);
    if (! err) {
      retval = 0;
    } else {
      fprintf(stderr, "%s image save failed\n", argv[0]);
    }
  } else {
    fprintf(stderr, "%s image new failed\n", argv[0]);
  }
  return retval;
}
