#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>
#include "mandelbrot_atom_domain_size.h"
#include "mandelbrot_external_ray_out.h"
#include "mpfr_complex.h"

const double pi = 3.141592653589793;
const double er = 1024;
const int resolution = 64;

void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s bits cx cy period\n"
    "cx,cy must be a nucleus of the given period"
    "outputs binary external angles on stdout\n"
    "example %s 53 -1 0 2\n",
    progname, progname);
}

char *external_ray_out(const mpfr_t cre, const mpfr_t cim, int dwell) {
  char *bits = calloc(1, dwell + 10);
  int nbits = 0;
  struct mandelbrot_external_ray_out *ray = mandelbrot_external_ray_out_new(cre, cim);
  while (mandelbrot_external_ray_out_step(ray)) {
    if (mandelbrot_external_ray_out_have_bit(ray)) {
      bits[nbits++] = '0' + mandelbrot_external_ray_out_get_bit(ray);
    }
  }
  mandelbrot_external_ray_out_delete(ray);
  return bits;
}

int main(int argc, char **argv) {
  if (argc != 5) { usage(argv[0]); return 1; }
  int prec = atoi(argv[1]);
  int period = atoi(argv[4]);
  if (period <= 1) {
    printf(".(0)\n.(1)\n");
    return 0;
  }
#define VARS nucleusx, nucleusy, r, probex, probey, zre, zim, t0, t1, mz, er2
  mpfr_t VARS;
  mpfr_inits2(prec, VARS, (mpfr_ptr) 0);
  mpfr_set_str(nucleusx, argv[2], 10, GMP_RNDN);
  mpfr_set_str(nucleusy, argv[3], 10, GMP_RNDN);
  mpfr_set_d(er2, er * er, GMP_RNDN);
  mandelbrot_atom_domain_size(r, nucleusx, nucleusy, period);
  mpfr_mul_d(r, r, 0.9, GMP_RNDN);
  int *counts = malloc(resolution * sizeof(int));
  int maxiter = 16 * period;
  for (int t = 0; t < resolution; ++t) {
    double a = 2 * pi * (t + 0.5) / resolution;
    double c = cos(a);
    double s = sin(a);
    // probe = nucleus + r * (c + I * s)
    mpfr_mul_d(probex, r, c, GMP_RNDN);
    mpfr_mul_d(probey, r, s, GMP_RNDN);
    cmpfr_add(probex, probey, probex, probey, nucleusx, nucleusy, GMP_RNDN);
    // z = 0;
    cmpfr_set_si(zre, zim, 0, 0, GMP_RNDN);
    int count = maxiter;
    for (int i = 0; i < maxiter; ++i) {
      // z = z * z + c;
      cmpfr_sqr(zre, zim, zre, zim, t0, t1, GMP_RNDN);
      cmpfr_add(zre, zim, zre, zim, probex, probey, GMP_RNDN);
      // mz = |z|^2
      cmpfr_abs2(mz, zre, zim, t0, t1, GMP_RNDN);
      if (mpfr_greater_p(mz, er2)) { count = i; break; }
    }
    counts[t] = count;
  }
  char *first = 0;
  while (1) {
    int mint = -1;
    int mincount = maxiter;
    for (int t = 0; t < resolution; ++t) {
      if (counts[t] < mincount) {
        mincount = counts[t];
        mint = t;
      }
    }
    if (mint == -1) {
      fprintf(stderr, "exhausted probes\n");
      return 1;
    }
    double a = 2 * pi * (mint + 0.5) / resolution;
    double c = cos(a);
    double s = sin(a);
    // probe = nucleus + r * (c + I * s)
    mpfr_mul_d(probex, r, c, GMP_RNDN);
    mpfr_mul_d(probey, r, s, GMP_RNDN);
    cmpfr_add(probex, probey, probex, probey, nucleusx, nucleusy, GMP_RNDN);
    char *bits = external_ray_out(probex, probey, mincount);
    int bitlen = strlen(bits);
    char *angle = malloc(period + 4);
    angle[0] = '.';
    angle[1] = '(';
    for (int i = 0; i < period; ++i) {
      angle[2 + i] = bits[bitlen-1 - i];
    }
    angle[2 + period] = ')';
    angle[3 + period] = 0;
    free(bits);
    if (first) {
      if (strcmp(first, angle) != 0) {
        if (strcmp(first, angle) < 0) {
          printf("%s\n%s\n", first, angle);
        } else {
          printf("%s\n%s\n", angle, first);
        }
        free(angle);
        break;
      }
    } else {
      first = angle;
      angle = 0;
    }
    counts[mint] = maxiter;
  }
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return 0;
}
