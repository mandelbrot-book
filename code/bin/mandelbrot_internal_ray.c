#include <complex.h>
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include "mandelbrot_interior.h"
#include "pi.h"

void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s bits nucleusx nucleusy period angle maxiters steps\n"
    "outputs space separated complex numbers on stdout\n"
    "example %s 53 0 0 1 1/3 100 1000\n",
    progname, progname);
}

int main(int argc, char **argv) {
  if (argc != 8) { usage(argv[0]); return 1; }
  int bits = atoi(argv[1]);
  mpfr_t nucleusx, nucleusy, boundaryx, boundaryy, wucleusx, wucleusy, interiorx, interiory;
  mpfr_inits2(bits, nucleusx, nucleusy, boundaryx, boundaryy, wucleusx, wucleusy, (mpfr_ptr) 0);
  mpfr_inits2(53, interiorx, interiory, (mpfr_ptr) 0);
  mpfr_set_str(nucleusx, argv[2], 10, GMP_RNDN);
  mpfr_set_str(nucleusy, argv[3], 10, GMP_RNDN);
  mpfr_set(wucleusx, nucleusx, GMP_RNDN);
  mpfr_set(wucleusy, nucleusy, GMP_RNDN);
  mpfr_set(boundaryx, nucleusx, GMP_RNDN);
  mpfr_set(boundaryy, nucleusy, GMP_RNDN);
  int period = atoi(argv[4]);
  mpq_t q;
  mpq_init(q);
  mpq_set_str(q, argv[5], 0);
  mpq_canonicalize(q);
  double angle = 2.0 * pi * mpq_get_d(q);
  complex double phase = cexp(I * angle);
  int maxiters = atoi(argv[6]);
  int steps = atoi(argv[7]);
  for (int step = 0; step < steps; ++step) {
    double radius = (step + 0.5) / steps;
    complex double interior = radius * phase;
    mpfr_set_d(interiorx, creal(interior), GMP_RNDN);
    mpfr_set_d(interiory, cimag(interior), GMP_RNDN);
    mandelbrot_interior(wucleusx, wucleusy, boundaryx, boundaryy, wucleusx, wucleusy, boundaryx, boundaryy, period, interiorx, interiory, maxiters);
    mpfr_out_str(0, 10, 0, boundaryx, GMP_RNDN);
    putchar(' ');
    mpfr_out_str(0, 10, 0, boundaryy, GMP_RNDN);
    putchar('\n');
  }
  mpq_clear(q);
  mpfr_clears(wucleusx, wucleusy, boundaryx, boundaryy, nucleusx, nucleusy, interiorx, interiory, (mpfr_ptr) 0);
  return 0;
}
