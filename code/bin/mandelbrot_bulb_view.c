#include <stdio.h>
#include <gmp.h>
#include <mpfr.h>
#include "mandelbrot_binary_angle.h"
#include "mandelbrot_external_ray_in.h"

void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s angle\n"
    "outputs bash script on stdout\n"
    "example %s 1/2\n",
    progname, progname);
}

int main(int argc, char **argv) {
  if (argc != 2) { usage(argv[0]); return 1; }
  mpq_t bulb;
  mpq_init(bulb);
  mpq_set_str(bulb, argv[1], 0);
  mpq_canonicalize(bulb);
  struct mandelbrot_binary_angle *lo, *hi, *tip, *tip2, *half, *half2;
  mandelbrot_binary_angle_bulb(&lo, &hi, bulb);
  half = mandelbrot_binary_angle_from_string(".1(0)");
  half2 = mandelbrot_binary_angle_other_representation(half);
  tip = mandelbrot_binary_angle_tune(lo, hi, half);
  tip2 = mandelbrot_binary_angle_tune(lo, hi, half2);
  mpq_t rlo, rhi, rtip, rtip2;
  mpq_init(rlo);
  mpq_init(rhi);
  mpq_init(rtip);
  mpq_init(rtip2);
  mandelbrot_binary_angle_to_rational(rlo, lo);
  mandelbrot_binary_angle_to_rational(rhi, hi);
  mandelbrot_binary_angle_to_rational(rtip, tip);
  mandelbrot_binary_angle_to_rational(rtip2, tip2);
  mpfr_t lox, loy;
  mpfr_init2(lox, 53);
  mpfr_init2(loy, 53);
  struct mandelbrot_external_ray_in *ray = mandelbrot_external_ray_in_new(rlo);
  for (int i = 0; i < 1000; ++i) {
    mandelbrot_external_ray_in_step(ray);
  }
  mandelbrot_external_ray_in_get(ray, lox, loy);
  mandelbrot_external_ray_in_delete(ray);
  mpfr_t hix, hiy;
  mpfr_init2(hix, 53);
  mpfr_init2(hiy, 53);
  ray = mandelbrot_external_ray_in_new(rhi);
  for (int i = 0; i < 1000; ++i) {
    mandelbrot_external_ray_in_step(ray);
  }
  mandelbrot_external_ray_in_get(ray, hix, hiy);
  mandelbrot_external_ray_in_delete(ray);
  mpfr_t tipx, tipy;
  mpfr_init2(tipx, 53);
  mpfr_init2(tipy, 53);
  ray = mandelbrot_external_ray_in_new(rtip);
  for (int i = 0; i < 1000; ++i) {
    mandelbrot_external_ray_in_step(ray);
  }
  mandelbrot_external_ray_in_get(ray, tipx, tipy);
  mandelbrot_external_ray_in_delete(ray);
  mpfr_t rootx, rooty;
  mpfr_init2(rootx, mpfr_get_prec(lox));
  mpfr_init2(rooty, mpfr_get_prec(lox));
  mpfr_add(rootx, lox, hix, GMP_RNDN);
  mpfr_add(rooty, loy, hiy, GMP_RNDN);
  mpfr_div_2si(rootx, rootx, 1, GMP_RNDN);
  mpfr_div_2si(rooty, rooty, 1, GMP_RNDN);
  mpfr_t cx, cy;
  mpfr_init2(cx, mpfr_get_prec(tipx));
  mpfr_init2(cy, mpfr_get_prec(tipx));
  mpfr_add(cx, rootx, tipx, GMP_RNDN);
  mpfr_add(cy, rooty, tipy, GMP_RNDN);
  mpfr_div_2si(cx, cx, 1, GMP_RNDN);
  mpfr_div_2si(cy, cy, 1, GMP_RNDN);
  mpfr_t dx, dy, d;
  mpfr_init2(dx, 53);
  mpfr_init2(dy, 53);
  mpfr_init2(d, 53);
  mpfr_sub(dx, cx, tipx, GMP_RNDN);
  mpfr_sub(dy, cy, tipy, GMP_RNDN);
  mpfr_sqr(dx, dx, GMP_RNDN);
  mpfr_sqr(dy, dy, GMP_RNDN);
  mpfr_add(d, dx, dy, GMP_RNDN);
  mpfr_sqrt(d, d, GMP_RNDN);
  mpfr_mul_d(d, d, 1.5, GMP_RNDN);
  printf(
    "#!/bin/bash\n"
    "view=\"");
  mpfr_out_str(0, 10, 0, cx, GMP_RNDN);
  putchar(' ');
  mpfr_out_str(0, 10, 0, cy, GMP_RNDN);
  putchar(' ');
  mpfr_out_str(0, 10, 0, d, GMP_RNDN);
  printf(
    "\"\n"
    "./render $view && ./colour > out.ppm && ./annotate out.ppm <<EOF\n"
    "rgba 1 1 1 1\n"
    "line ./ray_in ");
  mpq_out_str(0, 10, rlo);
  printf(
    " 250 | ./rescale 53 53 $view 0\n"
    "line ./ray_in ");
  mpq_out_str(0, 10, rhi);
  printf(
    " 250 | ./rescale 53 53 $view 0\n"
    "line ./ray_in ");
  mpq_out_str(0, 10, rtip);
  printf(
    " 250 | ./rescale 53 53 $view 0\n"
    "line ./ray_in ");
  mpq_out_str(0, 10, rtip2);
  printf(
    " 250 | ./rescale 53 53 $view 0\n"
    "EOF\n");
  mpfr_clears(lox, loy, hix, hiy, tipx, tipy, rootx, rooty, cx, cy, dx, dy, d, (mpfr_ptr) 0);
  mpq_clears(bulb, rlo, rhi, rtip, rtip2, (mpq_ptr) 0);
  mandelbrot_binary_angle_delete(lo);
  mandelbrot_binary_angle_delete(hi);
  mandelbrot_binary_angle_delete(tip);
  mandelbrot_binary_angle_delete(half);
  mandelbrot_binary_angle_delete(tip2);
  mandelbrot_binary_angle_delete(half2);
  return 0;
}
