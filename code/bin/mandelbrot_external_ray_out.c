#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpfr.h>
#include "mandelbrot_external_ray_out.h"

struct stack_item {
  struct stack_item *next;
  int value;
};

struct stack {
  struct stack_item *contents;
};

struct stack *stack_new() {
  struct stack *s = calloc(1, sizeof(struct stack));
  return s;
}

int stack_empty(struct stack *s) {
  return s->contents == 0;
}

void stack_push(struct stack *s, int v) {
  struct stack_item *i = calloc(1, sizeof(struct stack_item));
  i->next = s->contents;
  i->value = v;
  s->contents = i;
}

int stack_pop(struct stack *s) {
  struct stack_item *i = s->contents;
  int v = i->value;
  s->contents = i->next;
  free(i);
  return v;
}

void stack_delete(struct stack *s) {
  while (! stack_empty(s)) { stack_pop(s); }
  free(s);
}

void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s bits cre cim\n"
    "outputs space-separated complex numbers on stdout\n"
    "        followed by a blank line"
    "        then the external angle as a binary numerator\n"
    "warning no termination checking for interior\n"
    "example %s 53 0.26 0\n",
    progname, progname);
}

int main(int argc, char **argv) {
  // parse args
  if (argc != 4) { usage(argv[0]); return 1; }
  int bits = atoi(argv[1]);
  mpfr_t cre; mpfr_init2(cre, bits); mpfr_set_str(cre, argv[2], 10, GMP_RNDN);
  mpfr_t cim; mpfr_init2(cim, bits); mpfr_set_str(cim, argv[3], 10, GMP_RNDN);
  struct stack *bitstack = stack_new();
  struct mandelbrot_external_ray_out *ray = mandelbrot_external_ray_out_new(cre, cim);
  mpfr_t x, y;
  mpfr_init2(x, 53);
  mpfr_init2(y, 53);
  while (mandelbrot_external_ray_out_step(ray)) {
    if (mandelbrot_external_ray_out_have_bit(ray)) {
      stack_push(bitstack, mandelbrot_external_ray_out_get_bit(ray));
    }
    mandelbrot_external_ray_out_get(ray, x, y);
    mpfr_out_str(0, 10, 0, x, GMP_RNDN);
    putchar(' ');
    mpfr_out_str(0, 10, 0, y, GMP_RNDN);
    putchar('\n');
  }
  putchar('\n');
  while (! stack_empty(bitstack)) {
    int b = stack_pop(bitstack);
    putchar('0' + b);
  }
  putchar('\n');
  // cleanup
  stack_delete(bitstack);
  mpfr_clear(x);
  mpfr_clear(y);
  mpfr_clear(cre);
  mpfr_clear(cim);
  return 0;
}
