#define _POSIX_C_SOURCE 200809L
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct point {
  double x0;
  double y0;
  double x;
  double y;
  double u;
  double v;
};

void force_layout(struct point *p, int n, double sep) {
  double sep2 = sep * sep;
  bool converged = false;
  for (int m = 0; m < 1024 && ! converged; ++m) {
    for (int k = 0; k < n; ++k) {
      p[k].u = 0;
      p[k].v = 0;
    }
    converged = true;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        if (i == j) { continue; }
        double dx = p[i].x - p[j].x;
        double dy = p[i].y - p[j].y;
        double d2 = dx * dx + dy * dy;
        if (d2 > sep2) { continue; }
        if (! (d2 > 0)) { d2 = 1; }
        double d = sqrt(d2);
        p[i].u += dx / d * (sep - d);
        p[i].v += dy / d * (sep - d);
        converged = false;
      }
    }
    for (int k = 0; k < n; ++k) {
      p[k].x += p[k].u * sep * 0.1;
      p[k].y += p[k].v * sep * 0.1;
    }
  }
}

struct input {
  struct input *next;
  char *line;
};

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  int retval = 1;
  struct input *head = malloc(sizeof(struct input));
  head->next = 0;
  head->line = 0;
  struct input *cur = head;
  char *line = 0;
  size_t linelen = 0;
  while (1 < getline(&line, &linelen, stdin)) {
    struct input *in = malloc(sizeof(struct input));
    in->next = 0;
    in->line = strdup(line);
    cur->next = in;
    cur = in;
  }
  int count = 0;
  cur = head->next;
  while (cur) {
    if (strncmp(cur->line, "text ", strlen("text ")) == 0) {
      count++;
    }
    cur = cur->next;
  }
  struct point *points = malloc(count * sizeof(struct point));
  int k = 0;
  cur = head->next;
  while (cur) {
    if (strncmp(cur->line, "text ", strlen("text ")) == 0) {
      sscanf(cur->line + 5, "%lf %lf", &points[k].x0, &points[k].y0);
      points[k].x = points[k].x0;
      points[k].y = points[k].y0;
      k++;
    }
    cur = cur->next;
  }
  force_layout(points, count, 0.1);
  cur = head->next;
  k = 0;
  while (cur) {
    if (strncmp(cur->line, "text ", strlen("text ")) == 0) {
      double x0 = 0, y0 = 0;
      int n;
      sscanf(cur->line + 5, "%lf %lf %n", &x0, &y0, &n);
      printf("line /bin/echo -e \"%.18e %.18e\\n%.18e %.18e\"\n", points[k].x0, points[k].y0, points[k].x, points[k].y);
      printf("text %.18e %.18e %s", points[k].x, points[k].y, cur->line + 5 + n);
      k++;
    } else {
      printf("%s", cur->line);
    }
    cur = cur->next;
  }
  return retval;
}
