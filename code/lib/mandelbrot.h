#ifndef MANDELBROT_H
#define MANDELBROT_H 1

#include "mandelbrot_atom_domain_size.h"
#include "mandelbrot_binary_angle.h"
#include "mandelbrot_box_period.h"
#include "mandelbrot_external_ray_in.h"
#include "mandelbrot_external_ray_in_native.h"
#include "mandelbrot_external_ray_out.h"
#include "mandelbrot_interior.h"
#include "mandelbrot_nucleus.h"
#include "mandelbrot_parent.h"
#include "mandelbrot_shape.h"
#include "mandelbrot_size_estimate.h"
#include "mandelbrot_wucleus.h"

#include "mandelbrot_image.h"
#include "mandelbrot_render.h"

#include "mpfr_complex.h"
#include "pi.h"

#endif
