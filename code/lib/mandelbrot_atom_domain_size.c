#include "mandelbrot_atom_domain_size.h"
#include "mpfr_complex.h"

extern void mandelbrot_atom_domain_size(mpfr_t size, const mpfr_t cre, const mpfr_t cim, int p) {
  // allocate variables
#define VARS zre, zim, dcre, dcim, abszq2, abszp2, absdc2, t0, t1, t2, t3
  mpfr_t VARS;
  mpfr_inits2(mpfr_get_prec(cre), VARS, (mpfr_ptr) 0);
  // z = c;
  cmpfr_set(zre, zim, cre, cim, GMP_RNDN);
  // dc = 1;
  cmpfr_set_si(dcre, dcim, 1, 0, GMP_RNDN);
  // abszq = cabs(z);
  cmpfr_abs2(abszq2, zre, zim, t0, t1, GMP_RNDN);
  for (int q = 2; q <= p; ++q) {
    // dc = 2 * z * dc + 1;
    cmpfr_mul(dcre, dcim, zre, zim, dcre, dcim, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_mul_2si(dcre, dcim, dcre, dcim, 1, GMP_RNDN);
    mpfr_add_si(dcre, dcre, 1, GMP_RNDN);
    // z = z * z + c;
    cmpfr_sqr(zre, zim, zre, zim, t0, t1, GMP_RNDN);
    cmpfr_add(zre, zim, zre, zim, cre, cim, GMP_RNDN);
    // abszp = cabs(z);
    cmpfr_abs2(abszp2, zre, zim, t0, t1, GMP_RNDN);
    if (q < p && mpfr_less_p(abszp2, abszq2)) {
      // abszq2 = abszp2;
      mpfr_set(abszq2, abszp2, GMP_RNDN);
    }
  }
  // size = sqrt(abszq2) / cabs(dc);
  mpfr_sqrt(abszq2, abszq2, GMP_RNDN);
  cmpfr_abs2(absdc2, dcre, dcim, t0, t1, GMP_RNDN);
  mpfr_sqrt(absdc2, absdc2, GMP_RNDN);
  mpfr_div(size, abszq2, absdc2, GMP_RNDN);
  // cleanup
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
}
