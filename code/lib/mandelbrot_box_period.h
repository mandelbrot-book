#ifndef MANDELBROT_BOX_PERIOD_H
#define MANDELBROT_BOX_PERIOD_H 1

#include <mpfr.h>

extern int mandelbrot_box_period(const mpfr_t cx, const mpfr_t cy, const mpfr_t r, int maxperiod);

#endif
