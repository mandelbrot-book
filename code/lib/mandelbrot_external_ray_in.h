#ifndef MANDELBROT_EXTERNAL_RAY_IN
#define MANDELBROT_EXTERNAL_RAY_IN 1

#include <gmp.h>
#include <mpfr.h>

struct mandelbrot_external_ray_in;
extern struct mandelbrot_external_ray_in *mandelbrot_external_ray_in_new(mpq_t angle);
extern void mandelbrot_external_ray_in_delete(struct mandelbrot_external_ray_in *r);
extern int mandelbrot_external_ray_in_step(struct mandelbrot_external_ray_in *r);
extern void mandelbrot_external_ray_in_get(struct mandelbrot_external_ray_in *r, mpfr_t x, mpfr_t y);

#endif
