#ifndef MANDELBROT_RENDER_H
#define MANDELBROT_RENDER_H 1

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mandelbrot_image.h"

typedef int (*mandelbrot_progress_callback_t)(void *, double);

#include "mandelbrot_render_float.h"
#include "mandelbrot_render_double.h"
#include "mandelbrot_render_long_double.h"
#include "mandelbrot_render_mpfr.h"

#endif
