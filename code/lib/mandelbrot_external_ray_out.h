#ifndef MANDELBROT_EXTERNAL_RAY_OUT
#define MANDELBROT_EXTERNAL_RAY_OUT 1

#include <mpfr.h>

struct mandelbrot_external_ray_out;
extern struct mandelbrot_external_ray_out *mandelbrot_external_ray_out_new(const mpfr_t c0re, const mpfr_t c0im);
extern int mandelbrot_external_ray_out_step(struct mandelbrot_external_ray_out *r);
extern void mandelbrot_external_ray_out_delete(struct mandelbrot_external_ray_out *r);
extern int mandelbrot_external_ray_out_have_bit(struct mandelbrot_external_ray_out *r);
extern int mandelbrot_external_ray_out_get_bit(struct mandelbrot_external_ray_out *r);
extern void mandelbrot_external_ray_out_get(struct mandelbrot_external_ray_out *r, mpfr_t x, mpfr_t y);

#endif
