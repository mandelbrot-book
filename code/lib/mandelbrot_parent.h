#ifndef MANDELBROT_PARENT_H
#define MANDELBROT_PARENT_H 1

#include <gmp.h>
#include <mpfr.h>

extern int mandelbrot_parent(mpq_t angle, mpfr_t rootx, mpfr_t rooty, mpfr_t parentx, mpfr_t parenty, const mpfr_t nucleusx, const mpfr_t nucleusy, int period, int steps, const mpfr_t epsilon, int maxiters);

#endif
