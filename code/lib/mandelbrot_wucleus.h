#ifndef MANDELBROT_WUCLEUS_H
#define MANDELBROT_WUCLEUS_H 1

#include <mpfr.h>

extern int mandelbrot_wucleus(mpfr_t zre_out, mpfr_t zim_out, mpfr_t dzre_out, mpfr_t dzim_out, const mpfr_t z0re, const mpfr_t z0im, const mpfr_t cre, const mpfr_t cim, int period, const mpfr_t epsilon, int max_iters);

#endif
