#ifndef MANDELBROT_SHAPE_H
#define MANDELBROT_SHAPE_H 1

#include <mpfr.h>

enum mandelbrot_shape {
  mandelbrot_shape_cardioid,
  mandelbrot_shape_circle
};

extern enum mandelbrot_shape mandelbrot_shape(const mpfr_t cre, const mpfr_t cim, int period);

#endif
