#include "mandelbrot_box_period.h"

static int mandelbrot_crosses_positive_real_axis(const mpfr_t ax, const mpfr_t ay, const mpfr_t bx, const mpfr_t by) {
  if (mpfr_sgn(ay) != mpfr_sgn(by)) {
    mpfr_t dx, dy;
    mpfr_inits2(mpfr_get_prec(ax), dx, dy, (mpfr_ptr) 0);
    // d := b - a
    mpfr_sub(dx, bx, ax, GMP_RNDN);
    mpfr_sub(dy, by, ay, GMP_RNDN);
    int s = mpfr_sgn(dy);
    // dy := d `cross` a
    mpfr_mul(dy, dy, ax, GMP_RNDN);
    mpfr_mul(dx, dx, ay, GMP_RNDN);
    mpfr_sub(dy, dy, dx, GMP_RNDN);
    int t = mpfr_sgn(dy);
    mpfr_clear(dx);
    mpfr_clear(dy);
    return s == t;
  }
  return 0;
}

static int mandelbrot_surrounds_origin(const mpfr_t ax, const mpfr_t ay, const mpfr_t bx, const mpfr_t by, const mpfr_t cx, const mpfr_t cy, const mpfr_t dx, const mpfr_t dy) {
  return 1 & (
    mandelbrot_crosses_positive_real_axis(ax, ay, bx, by) +
    mandelbrot_crosses_positive_real_axis(bx, by, cx, cy) +
    mandelbrot_crosses_positive_real_axis(cx, cy, dx, dy) +
    mandelbrot_crosses_positive_real_axis(dx, dy, ax, ay) );
}

extern int mandelbrot_box_period(const mpfr_t cx, const mpfr_t cy, const mpfr_t r, int maxperiod) {
  // allocate box
  mpfr_prec_t prec = mpfr_get_prec(cx);
  mpfr_t v[18];
  for (int i = 0; i < 18; ++i) {
    mpfr_init2(v[i], prec);
  }
  // initialize box
  for (int i = 0; i < 8; ++i) {
    mpfr_set_si(v[i], 0, GMP_RNDN);
  }
  mpfr_sub(v[ 8], cx, r, GMP_RNDN);
  mpfr_sub(v[ 9], cy, r, GMP_RNDN);
  mpfr_add(v[10], cx, r, GMP_RNDN);
  mpfr_sub(v[11], cy, r, GMP_RNDN);
  mpfr_add(v[12], cx, r, GMP_RNDN);
  mpfr_add(v[13], cy, r, GMP_RNDN);
  mpfr_sub(v[14], cx, r, GMP_RNDN);
  mpfr_add(v[15], cy, r, GMP_RNDN);
  // iterate
  int period = 0;
  for (int p = 1; p <= maxperiod; ++p) {
    for (int i = 0; i < 8; i += 2) {
      // z := z^2 + c
      mpfr_sqr(v[16], v[i], GMP_RNDN);
      mpfr_sqr(v[17], v[i+1], GMP_RNDN);
      mpfr_mul(v[i+1], v[i], v[i+1], GMP_RNDN);
      mpfr_mul_2ui(v[i+1], v[i+1], 1, GMP_RNDN);
      mpfr_sub(v[i], v[16], v[17], GMP_RNDN);
      mpfr_add(v[i], v[i], v[i+8], GMP_RNDN);
      mpfr_add(v[i+1], v[i+1], v[i+9], GMP_RNDN);
      // FIXME check escaped?
    }
    if (mandelbrot_surrounds_origin(v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7])) {
      period = p;
      goto done;
    }
  }
done:
  for (int i = 0; i < 18; ++i) {
    mpfr_clear(v[i]);
  }
  return period;
}
