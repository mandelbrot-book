#include "mandelbrot_render.h"

#define FTYPE long double
#define FNAME(name) name ## l
#include "mandelbrot_render_native.c"
#undef FTYPE
#undef FNAME
