#ifndef MANDELBROT_EXTERNAL_RAY_IN_NATIVE
#define MANDELBROT_EXTERNAL_RAY_IN_NATIVE 1

#include <complex.h>
#include <stdbool.h>
#include <gmp.h>

struct mandelbrot_external_ray_in_native;
extern struct mandelbrot_external_ray_in_native *mandelbrot_external_ray_in_native_new(mpq_t angle);
extern void mandelbrot_external_ray_in_native_delete(struct mandelbrot_external_ray_in_native *r);
extern bool mandelbrot_external_ray_in_native_step(struct mandelbrot_external_ray_in_native *r);
extern complex double mandelbrot_external_ray_in_native_get(struct mandelbrot_external_ray_in_native *r);

#endif
