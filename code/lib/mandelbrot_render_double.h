#ifndef MANDELBROT_RENDER_DOUBLE_H
#define MANDELBROT_RENDER_DOUBLE_H 1

#include "mandelbrot_image.h"

#define FTYPE double
#define FNAME(name) name
#include "mandelbrot_render_native.h"
#undef FTYPE
#undef FNAME

#endif
