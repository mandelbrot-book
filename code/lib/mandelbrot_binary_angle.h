#ifndef MANDELBROT_BINARY_ANGLE_H
#define MANDELBROT_BINARY_ANGLE_H 1

#include <stdbool.h>
#include <gmp.h>

struct mandelbrot_binary_angle;
extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_from_string(const char *s);
extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_from_rational(const mpq_t r);
extern void mandelbrot_binary_angle_delete(struct mandelbrot_binary_angle *a);
extern char *mandelbrot_binary_angle_to_string(const struct mandelbrot_binary_angle *t);
extern void mandelbrot_binary_angle_to_rational(mpq_t r, const struct mandelbrot_binary_angle *t);
extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_canonicalize(const struct mandelbrot_binary_angle *t);
extern bool mandelbrot_binary_angle_is_canonical(const struct mandelbrot_binary_angle *a);
extern int mandelbrot_binary_angle_preperiod(const struct mandelbrot_binary_angle *t);
extern int mandelbrot_binary_angle_period(const struct mandelbrot_binary_angle *t);
extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_other_representation(const struct mandelbrot_binary_angle *t);
extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_tune(const struct mandelbrot_binary_angle *lo, const struct mandelbrot_binary_angle *hi, const struct mandelbrot_binary_angle *t);
extern void mandelbrot_binary_angle_bulb(struct mandelbrot_binary_angle **lo, struct mandelbrot_binary_angle **hi, const mpq_t b);

#endif
