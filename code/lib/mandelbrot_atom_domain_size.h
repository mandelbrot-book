#ifndef MANDELBROT_ATOM_DOMAIN_SIZE_H
#define MANDELBROT_ATOM_DOMAIN_SIZE_H 1

#include <mpfr.h>

extern void mandelbrot_atom_domain_size(mpfr_t size, const mpfr_t cre, const mpfr_t cim, int p);

#endif
