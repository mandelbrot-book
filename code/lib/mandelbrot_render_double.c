#include "mandelbrot_render.h"

#define FTYPE double
#define FNAME(name) name
#include "mandelbrot_render_native.c"
#undef FTYPE
#undef FNAME
