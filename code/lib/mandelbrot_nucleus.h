#ifndef MANDELBROT_NUCLEUS_H
#define MANDELBROT_NUCLEUS_H 1

#include <mpfr.h>

extern int mandelbrot_nucleus(mpfr_t cx, mpfr_t cy, const mpfr_t c0x, const mpfr_t c0y, int period, int maxiters);

#endif
