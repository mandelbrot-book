#include "mandelbrot_render.h"

#define FTYPE float
#define FNAME(name) name ## f
#include "mandelbrot_render_native.c"
#undef FTYPE
#undef FNAME
