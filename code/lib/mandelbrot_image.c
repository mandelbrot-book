#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mandelbrot_image.h"
#include "pi.h"

extern struct mandelbrot_image *mandelbrot_image_new(int width, int height) {
  struct mandelbrot_image *img = calloc(1, sizeof(*img));
  if (! img) { return 0; }
  img->width = width;
  img->height = height;
  img->pixels = calloc(width * height, sizeof(*img->pixels));
  if (! img->pixels) { free(img); return 0; }
  return img;
}

extern void mandelbrot_image_free(struct mandelbrot_image *img) {
  free(img->pixels);
  free(img);
}

extern int mandelbrot_image_save(struct mandelbrot_image *img, char *filestem, float escape_radius) {
  int retval = 0;

  int length = strlen(filestem) + 100;
  char *filename = calloc(length, 1);
  if (filename) {

    snprintf(filename, length, "%s_t.ppm", filestem);
    FILE *f = fopen(filename, "wb");
    if (f) {
      fprintf(f, "P6\n%d %d\n255\n", img->width, img->height);
      for (int j = 0; j < img->height; ++j) {
        for (int i = 0; i < img->width; ++i) {
          int n = 0;
          switch (img->pixels[j * img->width + i].tag) {
            case mandelbrot_pixel_exterior:
              n = 255;
              break;
            case mandelbrot_pixel_interior:
              n = 0;
              break;
            case mandelbrot_pixel_unescaped:
              n = 128;
              break;
          }
          fputc(n, f);
          fputc(n, f);
          fputc(n, f);
        }
      }
      fclose(f);
    } else {
      retval = 1;
    }

    snprintf(filename, length, "%s_n.ppm", filestem);
    f = fopen(filename, "wb");
    if (f) {
      fprintf(f, "P6\n%d %d\n255\n", img->width, img->height);
      for (int j = 0; j < img->height; ++j) {
        for (int i = 0; i < img->width; ++i) {
          int n = 0;
          switch (img->pixels[j * img->width + i].tag) {
            case mandelbrot_pixel_exterior:
              n = img->pixels[j * img->width + i].u.exterior.final_n;
              break;
          }
          fputc(n >> 16, f);
          fputc(n >>  8, f);
          fputc(n      , f);
        }
      }
      fclose(f);
    } else {
      retval = 1;
    }

    snprintf(filename, length, "%s_z_abs.ppm", filestem);
    f = fopen(filename, "wb");
    if (f) {
      fprintf(f, "P6\n%d %d\n255\n", img->width, img->height);
      for (int j = 0; j < img->height; ++j) {
        for (int i = 0; i < img->width; ++i) {
          complex float z = 0;
          float r = 0;
          switch (img->pixels[j * img->width + i].tag) {
            case mandelbrot_pixel_exterior:
              z = img->pixels[j * img->width + i].u.exterior.final_z;
              r = logf(cabsf(z)) / logf(escape_radius) - 1.0f;
              break;
            case mandelbrot_pixel_interior:
              z = img->pixels[j * img->width + i].u.interior.final_dz;
              r = cabsf(z);
              break;
          }
          int n = (1 << 24) * r;
          fputc(n >> 16, f);
          fputc(n >>  8, f);
          fputc(n      , f);
        }
      }
      fclose(f);
    } else {
      retval = 1;
    }

    snprintf(filename, length, "%s_z_arg.ppm", filestem);
    f = fopen(filename, "wb");
    if (f) {
      fprintf(f, "P6\n%d %d\n255\n", img->width, img->height);
      for (int j = 0; j < img->height; ++j) {
        for (int i = 0; i < img->width; ++i) {
          complex float z = 0;
          float t = 0;
          switch (img->pixels[j * img->width + i].tag) {
            case mandelbrot_pixel_exterior:
              z = img->pixels[j * img->width + i].u.exterior.final_z;
              t = fmodf(cargf(z) / (2.0f * pif) + 1.0f, 1.0f);
              break;
            case mandelbrot_pixel_interior:
              z = img->pixels[j * img->width + i].u.interior.final_dz;
              t = fmodf(cargf(z) / (2.0f * pif) + 1.0f, 1.0f);
              break;
          }
          int n = (1 << 24) * t;
          fputc(n >> 16, f);
          fputc(n >>  8, f);
          fputc(n      , f);
        }
      }
      fclose(f);
    } else {
      retval = 1;
    }

    snprintf(filename, length, "%s_de.ppm", filestem);
    f = fopen(filename, "wb");
    if (f) {
      fprintf(f, "P6\n%d %d\n255\n", img->width, img->height);
      for (int j = 0; j < img->height; ++j) {
        for (int i = 0; i < img->width; ++i) {
          float de = 1 << 12;
          switch (img->pixels[j * img->width + i].tag) {
            case mandelbrot_pixel_exterior:
            {
              de = img->pixels[j * img->width + i].u.exterior.final_de;
              break;
            }
            case mandelbrot_pixel_interior:
            {
              de = img->pixels[j * img->width + i].u.interior.final_de;
              break;
            }
          }
          int n = ((1 << 24) - 1) * fminf(fmaxf(de / (1 << 12), 0.0f), 1.0f);
          fputc(n >> 16, f);
          fputc(n >>  8, f);
          fputc(n      , f);
        }
      }
      fclose(f);
    } else {
      retval = 1;
    }

    snprintf(filename, length, "%s_p.ppm", filestem);
    f = fopen(filename, "wb");
    if (f) {
      fprintf(f, "P6\n%d %d\n255\n", img->width, img->height);
      for (int j = 0; j < img->height; ++j) {
        for (int i = 0; i < img->width; ++i) {
          int n = 0;
          switch (img->pixels[j * img->width + i].tag) {
            case mandelbrot_pixel_exterior:
              n = img->pixels[j * img->width + i].u.exterior.final_p;
              break;
            case mandelbrot_pixel_interior:
              n = img->pixels[j * img->width + i].u.interior.final_p;
              break;
          }
          fputc(n >> 16, f);
          fputc(n >>  8, f);
          fputc(n      , f);
        }
      }
      fclose(f);
    } else {
      retval = 1;
    }

    free(filename);
  } else {
    retval = 1;
  }

  return retval;
}

extern struct mandelbrot_pixel *mandelbrot_image_peek(struct mandelbrot_image *img, int i, int j) {
  return &img->pixels[j * img->width + i];
}

extern int mandelbrot_image_in_bounds(struct mandelbrot_image *img, int i, int j) {
  return 0 <= i && i < img->width && 0 <= j && j < img->height;
}
