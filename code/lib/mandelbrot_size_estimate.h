#ifndef MANDELBROT_SIZE_ESTIMATE_H
#define MANDELBROT_SIZE_ESTIMATE_H 1

#include <mpfr.h>

extern void mandelbrot_size_estimate(mpfr_t sx, mpfr_t sy, const mpfr_t cx, const mpfr_t cy, int period);

#endif
