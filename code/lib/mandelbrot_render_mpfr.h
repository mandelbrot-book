#ifndef MANDELBROT_RENDER_MPFR_H
#define MANDELBROT_RENDER_MPFR_H 1

#include <mpfr.h>
#include "mandelbrot_image.h"

extern void mandelbrot_render_mpfr(struct mandelbrot_image *img, const mpfr_t cre, const mpfr_t cim, const mpfr_t radius, int maximum_iterations, const mpfr_t escape_radius2, const mpfr_t pixel_spacing, int interior, void *userdata, mandelbrot_progress_callback_t progresscb);

#endif
