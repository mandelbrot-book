#include "mpfr_complex.h"

extern void cmpfr_set(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, mpfr_rnd_t rnd) {
  mpfr_set(r_re, x_re, rnd);
  mpfr_set(r_im, x_im, rnd);
}
          
extern void cmpfr_set_si(mpfr_t r_re, mpfr_t r_im, int x_re, int x_im, mpfr_rnd_t rnd) {
  mpfr_set_si(r_re, x_re, rnd);
  mpfr_set_si(r_im, x_im, rnd);
}
          
extern void cmpfr_mul(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, const mpfr_t y_re, const mpfr_t y_im, mpfr_t t0, mpfr_t t1, mpfr_t t2, mpfr_t t3, mpfr_rnd_t rnd) {
  mpfr_mul(t0, x_re, y_re, rnd);
  mpfr_mul(t1, x_im, y_im, rnd);
  mpfr_mul(t2, x_re, y_im, rnd);
  mpfr_mul(t3, x_im, y_re, rnd);
  mpfr_sub(r_re, t0, t1, rnd);
  mpfr_add(r_im, t2, t3, rnd);
}

extern void cmpfr_mul_2si(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, int y, mpfr_rnd_t rnd) {
  mpfr_mul_2si(r_re, x_re, y, rnd);
  mpfr_mul_2si(r_im, x_im, y, rnd);
}

extern void cmpfr_sqr(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, mpfr_t t0, mpfr_t t1, mpfr_rnd_t rnd) {
  mpfr_sqr(t0, x_re, rnd);
  mpfr_sqr(t1, x_im, rnd);
  mpfr_mul(r_im, x_re, x_im, rnd);
  mpfr_mul_2si(r_im, r_im, 1, rnd);
  mpfr_sub(r_re, t0, t1, rnd);
}

extern void cmpfr_add(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, const mpfr_t y_re, const mpfr_t y_im, mpfr_rnd_t rnd) {
  mpfr_add(r_re, x_re, y_re, rnd);
  mpfr_add(r_im, x_im, y_im, rnd);
}

extern void cmpfr_sub(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, const mpfr_t y_re, const mpfr_t y_im, mpfr_rnd_t rnd) {
  mpfr_sub(r_re, x_re, y_re, rnd);
  mpfr_sub(r_im, x_im, y_im, rnd);
}

extern void cmpfr_sub_d(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, double y_re, double y_im, mpfr_rnd_t rnd) {
  mpfr_sub_d(r_re, x_re, y_re, rnd);
  mpfr_sub_d(r_im, x_im, y_im, rnd);
}

extern void cmpfr_div(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, const mpfr_t y_re, const mpfr_t y_im, mpfr_t t0, mpfr_t t1, mpfr_t t2, mpfr_t t3, mpfr_rnd_t rnd) {
  mpfr_mul(t0, x_re, y_re, rnd);
  mpfr_mul(t1, x_im, y_im, rnd);
  mpfr_mul(t2, x_re, y_im, rnd);
  mpfr_mul(t3, x_im, y_re, rnd);
  mpfr_add(r_re, t0, t1, rnd);
  mpfr_sub(r_im, t3, t2, rnd);
  mpfr_sqr(t0, y_re, rnd);
  mpfr_sqr(t1, y_im, rnd);
  mpfr_add(t2, t0, t1, rnd);
  mpfr_div(r_re, r_re, t2, rnd);
  mpfr_div(r_im, r_im, t2, rnd);
}

extern void cmpfr_abs2(mpfr_t r, const mpfr_t x_re, const mpfr_t x_im, mpfr_t t0, mpfr_t t1, mpfr_rnd_t rnd) {
  mpfr_sqr(t0, x_re, rnd);
  mpfr_sqr(t1, x_im, rnd);
  mpfr_add(r, t0, t1, rnd);
}
