#!/bin/bash
OUT="tmp"
mkdir -p "${OUT}"
(
cat <<"EOF"
\documentclass{book}
%\usepackage[paperwidth=155mm,paperheight=235mm]{geometry}
\usepackage[paperwidth=210mm,paperheight=297mm]{geometry}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amssymb}
\usepackage{MnSymbol}
\usepackage{algorithm}
\usepackage{multirow}
\usepackage[detect-all]{siunitx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{morefloats}
\usepackage[normalem]{ulem}

\newcommand{\M}{$M$}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}

\newcommand{\dd}[1]{\frac{\partial}{\partial{#1}}}
\newcommand{\DD}[1]{\llangle#1\rrangle}

\usepackage{listings}
\lstdefinelanguage{DIFF}{
  morecomment=[f][\it][0]{@@},
  morecomment=[f][\sout][0]{-}
}[]
\newcommand{\listDIFFC}[1]{%
\begin{figure}[ht]%
\lstset{language=C, alsolanguage=DIFF, morekeywords={complex}, basicstyle=\small, numbers=left, stepnumber=5, numberstyle=\tiny, firstnumber=1, frame=leftline}%
\lstinputlisting[firstline=3]{#1}%
\end{figure}%
}

\numberwithin{algorithm}{section}
\numberwithin{figure}{section}
\numberwithin{table}{section}

\begin{document}

\title{How To Write A Book \\
About The Mandelbrot Set}
\author{Claude Heiland-Allen}
\date{\today}

\maketitle
%\input{book/title.tex}

\tableofcontents
\listofalgorithms
\listoffigures
\lstlistoflistings
\listoftables

\part{Imaging \M}

\chapter{The Exterior}

EOF
git log --reverse --pretty=oneline |
while read commit title
do
   git diff -U1 "${commit}~1" "${commit}" > "${OUT}/${commit}.diff"
   echo "\\section{${title}}"
   git log -n1 "${commit}" | tail -n+7
   echo "\\listDIFFC{${commit}.diff}"
done
cat <<EOF

\end{document}
EOF
) > "${OUT}/log.tex"
