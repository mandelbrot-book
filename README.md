# mandelbrot-book

This repository is deprecated.

For text, see
<https://mathr.co.uk/web/mandelbrot.html>.

For code, see
<https://code.mathr.co.uk/mandelbrot-numerics>,
<https://code.mathr.co.uk/mandelbrot-symbolics>,
<https://code.mathr.co.uk/mandelbrot-perturbator>,
and others.
